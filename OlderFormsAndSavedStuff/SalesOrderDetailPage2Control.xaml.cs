﻿using System;

using GPayProductionApp.Models;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace GPayProductionApp2.Views
{
    public sealed partial class SalesOrderDetailPage2Control : UserControl
    {
        public SalesOrders MasterMenuItem
        {
            get { return GetValue(MasterMenuItemProperty) as SalesOrders; }
            set { SetValue(MasterMenuItemProperty, value); }
        }

        public static readonly DependencyProperty MasterMenuItemProperty = DependencyProperty.Register("MasterMenuItem", typeof(SalesOrders), typeof(SalesOrderDetailPage2Control), new PropertyMetadata(null, OnMasterMenuItemPropertyChanged));

        public SalesOrderDetailPage2Control()
        {
            InitializeComponent();
        }

        private static void OnMasterMenuItemPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as SalesOrderDetailPage2Control;
            control.ForegroundElement.ChangeView(0, 0, 1);
        }
    }
}
