﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;

//using SDKTemplate;
//using System;

using System.Threading.Tasks;
using System.IO;
using System.Text;

using Windows.Networking;

using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.System;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.ApplicationModel;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage.AccessCache;
using Windows.UI.Xaml.Media;
using System.Collections.Generic;


namespace GPayProductionApp2.Views
{
    public sealed partial class TestPage : Page, INotifyPropertyChanged
    {
        // A pointer back to the main page is required to display status messages.
        private MainPage rootPage;

        string sResponse;
        String host = "192.168.2.181";
        String sStartupMsg = "";
        String sSendMsg = "#Status\r\n";
        String sRcvMsg = "";

        SocketClientSync sc;
        
        public TestPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            rootPage = MainPage.Current;

            // Attach handlers for suspension to stop the watcher when the App is suspended.
            App.Current.Suspending += App_Suspending;
            App.Current.Resuming += App_Resuming;

            rootPage.NotifyUser("Press Run to start watcher.", MainPage.NotifyType.StatusMessage);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            // Remove local suspension handlers from the App since this page is no longer active.
            App.Current.Suspending -= App_Suspending;
            App.Current.Resuming -= App_Resuming;

            rootPage.NotifyUser("Navigating away. Watcher stopped.", MainPage.NotifyType.StatusMessage);

            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {
            rootPage.NotifyUser("App suspending. Watcher stopped.", MainPage.NotifyType.StatusMessage);
        }

        private void App_Resuming(object sender, object e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));


        private async void Test_Click(object sender, RoutedEventArgs e)
        {
            /*
            //Username and or password is incorrect
            ContentDialog noUserPwd = new ContentDialog();
            noUserPwd.Title = "Entry incorrect";
            noUserPwd.Content = "User already exists";
            noUserPwd.CloseButtonText = "OK";

            await noUserPwd.ShowAsync();
            */
        }

        public class SocketClient
        {
            StreamSocket socket = null;


            /// <summary>
            /// CONNECT TO SERVER
            /// </summary>
            /// <param name="host">Host name/IP address</param>
            /// <param name="port">Port number</param>
            /// <param name="message">Message to server</param>
            /// <returns>Response from server</returns>
            public async Task connect(string host, string port, string message)
            {
                HostName hostName;

                if (socket == null)
                    socket = new StreamSocket();

                using (socket)
                {
                    hostName = new HostName(host);

                    // Set NoDelay to false so that the Nagle algorithm is not disabled
                    socket.Control.NoDelay = false;
                    //socket.Control.KeepAlive = true;

                    try
                    {
                        // Connect to the server
                        await socket.ConnectAsync(hostName, port);
                        await this.read();
                        // Send the message
                        await this.send("#Status");
                        await this.read();
                    }
                    catch (Exception exception)
                    {
                        switch (SocketError.GetStatus(exception.HResult))
                        {
                            case SocketErrorStatus.HostNotFound:
                                // Handle HostNotFound Error
                                throw;
                            default:
                                // If this is an unknown status it means that the error is fatal and retry will likely fail.
                                throw;
                        }
                    }
                }
            }

            /// <summary>
            /// SEND DATA
            /// </summary>
            /// <param name="message">Message to server</param>
            /// <returns>void</returns>
            public async Task send(string message)
            {
                // Create the data writer object backed by the in-memory stream. 
                using (DataWriter writer = new DataWriter(socket.OutputStream))
                {
                    // Set the Unicode character encoding for the output stream
                    //writer.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;
                    // Specify the byte order of a stream.
                    //writer.ByteOrder = Windows.Storage.Streams.ByteOrder.LittleEndian;

                    // Gets the size of UTF-8 string.
                    writer.MeasureString(message);
                    // Write a string value to the output stream.
                    writer.WriteString(message);

                    // Send the contents of the writer to the backing stream.
                    try
                    {
                        await writer.StoreAsync();
                    }
                    catch (Exception exception)
                    {
                        /*
                        switch (SocketError.GetStatus(exception.HResult))
                        {
                            case SocketErrorStatus.HostNotFound:
                                // Handle HostNotFound Error
                                throw;
                            default:
                                // If this is an unknown status it means that the error is fatal and retry will likely fail.
                                throw;
                        }
                        */
                    }

                    await writer.FlushAsync();
                    // In order to prolong the lifetime of the stream, detach it from the DataWriter
                    writer.DetachStream();
                }
            }

            /// <summary>
            /// READ RESPONSE
            /// </summary>
            /// <returns>Response from server</returns>
            public async Task<String> read()
            {
                StringBuilder strBuilder;

                using (DataReader reader = new DataReader(socket.InputStream))
                {
                    strBuilder = new StringBuilder();

                    // Set the DataReader to only wait for available data (so that we don't have to know the data size)
                    reader.InputStreamOptions = Windows.Storage.Streams.InputStreamOptions.Partial;

                    // The encoding and byte order need to match the settings of the writer we previously used.
                    //reader.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;

                    //reader.ByteOrder = Windows.Storage.Streams.ByteOrder.LittleEndian;


                    try
                    {
                        /*                    while(true)
                                            {
                                                // Send the contents of the writer to the backing stream. 
                                                // Get the size of the buffer that has not been read.
                                                uint sizeFieldCount = await reader.LoadAsync(sizeof(uint));

                                                if (sizeFieldCount != sizeof(uint))
                                                {
                                                    // The underlying socket was closed before we were able to read the whole data.
                                                    return strBuilder.ToString();
                                                }

                                                // Read the string.
                                                uint stringLength = reader.ReadUInt32();
                                                uint actualStringLength = await reader.LoadAsync(stringLength);
                                                strBuilder.Append(reader.ReadString(actualStringLength));

                                                if (stringLength != actualStringLength)
                                                    return strBuilder.ToString();

                                                return strBuilder.ToString();
                                            }
                                            */
                        uint stringLength = await reader.LoadAsync(512);

                        strBuilder.Append(reader.ReadString(stringLength));
                    }
                    catch (Exception exception)
                    {
                        /*
                        switch (SocketError.GetStatus(exception.HResult))
                        {
                            case SocketErrorStatus.HostNotFound:
                                // Handle HostNotFound Error
                                throw;
                            default:
                                // If this is an unknown status it means that the error is fatal and retry will likely fail.
                                throw;
                        }
                        */
                    }

                    reader.DetachStream();
                    return strBuilder.ToString();
                }
            }
        }

        public class SocketClientSync
        {
            StreamSocket clientSocket = null;


            /// <summary>
            /// CONNECT TO SERVER
            /// </summary>
            /// <param name="host">Host name/IP address</param>
            /// <param name="port">Port number</param>
            /// <param name="message">Message to server</param>
            /// <returns>Response from server</returns>
            public async Task<string> connect(string host, string port)
            {
                HostName hostName;
                String sStartupMsg = "";

                if (clientSocket == null)
                    clientSocket = new StreamSocket();


                hostName = new HostName(host);

                // Set NoDelay to false so that the Nagle algorithm is not disabled
                clientSocket.Control.NoDelay = false;
                //socket.Control.KeepAlive = true;

                try
                {
                    await clientSocket.ConnectAsync(hostName, port);
                    sStartupMsg = await receive();
                }
                catch (Exception exception)
                {
                    // If this is an unknown status, 
                    // it means that the error is fatal and retry will likely fail.
                    if (SocketError.GetStatus(exception.HResult) == SocketErrorStatus.Unknown)
                    {
                        throw;
                    }
                    // the Close method is mapped to the C# Dispose
                    clientSocket.Dispose();
                    //clientSocket = null;
                }

                return sStartupMsg;
            }

            public async Task<String> receive()
            {
                String sReceivedMessage = "";

                // Now try to receive data from server
                try
                {
                    DataReader reader = new DataReader(clientSocket.InputStream);
                    // Set inputstream options so that we don't have to know the data size
                    reader.InputStreamOptions = InputStreamOptions.Partial;
                    uint sLength = await reader.LoadAsync(4096);
                    sReceivedMessage = reader.ReadString(sLength);

                    reader.DetachStream();
                    reader.Dispose();
                }
                catch (Exception exception)
                {
                    // If this is an unknown status, 
                    // it means that the error is fatal and retry will likely fail.
                    if (SocketError.GetStatus(exception.HResult) == SocketErrorStatus.Unknown)
                    {
                        throw;
                    }


                    clientSocket.Dispose();
                    clientSocket = null;


                }

                return sReceivedMessage;
            }

            public async Task send(string message)
            {
                try
                {
                    DataWriter writer = new DataWriter(clientSocket.OutputStream);

                    writer.ByteOrder = ByteOrder.BigEndian;
                    writer.UnicodeEncoding = Windows.Storage.Streams.UnicodeEncoding.Utf8;

                    uint len = writer.MeasureString(message); // Gets the UTF-8 string length.

                    writer.WriteString(message);

                    // Call StoreAsync method to store the data to a backing stream
                    await writer.StoreAsync();

                    await writer.FlushAsync();

                    // detach the stream and close it
                    writer.DetachStream();
                    writer.Dispose();

                }
                catch (Exception exception)
                {
                    // If this is an unknown status, 
                    // it means that the error is fatal and retry will likely fail.
                    if (SocketError.GetStatus(exception.HResult) == SocketErrorStatus.Unknown)
                    {
                        throw;
                    }


                    clientSocket.Dispose();
                    clientSocket = null;
                }
            }

            public bool disconnect()
            {
                clientSocket.Dispose();
                clientSocket = null;
                return true;
            }
        }

        public enum NotifyType
        {
            StatusMessage,
            ErrorMessage
        };

        public void NotifyUser(string strMessage, NotifyType type)
        {
            // If called from the UI thread, then update immediately.
            // Otherwise, schedule a task on the UI thread to perform the update.
            if (Dispatcher.HasThreadAccess)
            {
                UpdateStatus(strMessage, type);
            }
            else
            {
                var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UpdateStatus(strMessage, type));
            }
        }

        private void UpdateStatus(string strMessage, NotifyType type)
        {
            switch (type)
            {
                case NotifyType.StatusMessage:
                    StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                    break;
                case NotifyType.ErrorMessage:
                    StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Red);
                    break;
            }

            StatusBlock.Text = strMessage;

            // Collapse the StatusBlock if it has no text to conserve real estate.
            StatusBorder.Visibility = (StatusBlock.Text != String.Empty) ? Visibility.Visible : Visibility.Collapsed;
            if (StatusBlock.Text != String.Empty)
            {
                StatusBorder.Visibility = Visibility.Visible;
                StatusPanel.Visibility = Visibility.Visible;
            }
            else
            {
                StatusBorder.Visibility = Visibility.Collapsed;
                StatusPanel.Visibility = Visibility.Collapsed;
            }

            // Raise an event if necessary to enable a screen reader to announce the status update.
//            var peer = FrameworkElementAutomationPeer.FromElement(StatusBlock);
//            if (peer != null)
//            {
//                peer.RaiseAutomationEvent(AutomationEvents.LiveRegionChanged);
//            }
        }

        private void NotifyUserFromAsyncThread(string strMessage, NotifyType type)
        {
            //var ignore = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => rootPage.NotifyUser(strMessage, type));
            NotifyUser(strMessage, type);
        }

        private void Disconnect_Click(object sender, RoutedEventArgs e)
        {
            sc.disconnect();
        }

        public static Task<bool> ExecuteBatchFile(string BatchFilePath, string BatchFileDirectory)
        {
            try
            {
                Task<bool> executeBatchFileTask = Task.Run<bool>(() =>
                {
                    bool hasProcessExited = false;
                    ProcessStartInfo startInfo = new ProcessStartInfo()
                    {
                        FileName = BatchFilePath,
                        CreateNoWindow = false,
                        UseShellExecute = true,
                        WindowStyle = ProcessWindowStyle.Normal,
                        WorkingDirectory = BatchFileDirectory
                    };

                    // Start the process with the info we specified.
                    // Call WaitForExit and then the using-statement will close.
                    using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startInfo))
                    {
                        while (!exeProcess.HasExited)
                        {
                            //Do nothing  
                        }
                        hasProcessExited = true;
                    }

                    return hasProcessExited;
                });
                return executeBatchFileTask;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static async Task LaunchExeAsync(string exeId, bool prompt = true)
        {
            //var openPicker = new FileOpenPicker();
            //openPicker.ViewMode = PickerViewMode.Thumbnail;
            //openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary; // PickerLocationId.DocumentsLibrary;
            //openPicker.FileTypeFilter.Add(".bat");
            //openPicker.FileTypeFilter.Add(".png");
            //openPicker.FileTypeFilter.Add(".jpg");
            //openPicker.FileTypeFilter.Add(".jpeg");

            //StorageFile file = await openPicker.PickSingleFileAsync();


            //exeId = "." + exeId;
            StorageFolder storageFolder = KnownFolders.PicturesLibrary;
            //StorageFolder storageFolder = KnownFolders.DocumentsLibrary;
            //StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile file = null;

            try 
            {
                file = await storageFolder.GetFileAsync(exeId);
                //file = await ApplicationData.Current.LocalFolder.GetFileAsync(exeId);
            }
            catch (FileNotFoundException)
            {

            }
            
            LauncherOptions launchopt = new LauncherOptions();
            launchopt.DisplayApplicationPicker = true;
            launchopt.TreatAsUntrusted = true;


            //bool x = await Launcher.LaunchFileAsync(file, launchopt);
            bool x = await Launcher.LaunchFileAsync(file);
        }

        private async void SendCommand_Click(object sender, RoutedEventArgs e)
        {
            sSendMsg = Command.Text + "\r\n";
            await sc.send(sSendMsg);

            // Display the string on the screen. The event is invoked on a non-UI thread, so we need to marshal
            // the text back to the UI thread.
            // NotifyUserFromAsyncThread(
            //   String.Format("Send data: \"{0}\"", sSendMsg),
            // NotifyType.StatusMessage);
            await Task.Delay(TimeSpan.FromMilliseconds(100));

            /*
            sRcvMsg = await sc.receive();

            // Display the string on the screen. The event is invoked on a non-UI thread, so we need to marshal
            // the text back to the UI thread.
            NotifyUserFromAsyncThread(
              String.Format("Received data: \"{0}\"", sRcvMsg),
            NotifyType.StatusMessage);

            await Task.Delay(TimeSpan.FromMilliseconds(100));

            sRcvMsg = await sc.receive();

            // Display the string on the screen. The event is invoked on a non-UI thread, so we need to marshal
            // the text back to the UI thread.
            NotifyUserFromAsyncThread(
              String.Format("Received data: \"{0}\"", sRcvMsg),
            NotifyType.StatusMessage);
            */
        }

        private async void ReceiveCommand_Click(object sender, RoutedEventArgs e)
        {
            sRcvMsg = await sc.receive();

            // Display the string on the screen. The event is invoked on a non-UI thread, so we need to marshal
            // the text back to the UI thread.
            NotifyUserFromAsyncThread(
              String.Format("Received data: \"{0}\"", sRcvMsg),
            NotifyType.StatusMessage);
        }

        private void Command_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private async void ConnectTcp_Click(object sender, RoutedEventArgs e)
        {
            sc = new SocketClientSync();

            string sStartupMsg = await sc.connect(host, "23");

            // Display the string on the screen. The event is invoked on a non-UI thread, so we need to marshal
            // the text back to the UI thread.
            NotifyUserFromAsyncThread(String.Format("Received data: \"{0}\"", sStartupMsg), NotifyType.StatusMessage);
            return;
        }

        private async void RunEraseProgram_Click(object sender, RoutedEventArgs e)
        {
            string sResult = "";

            RunEraseProgram.IsEnabled = false;

            Clipboard.Clear();

            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("JFlasherEraseProgramGroup");

            do
            {
                await Task.Delay(1000);
                sResult = await FetchClipboard();
            }
            while (String.Compare(sResult, "", true) == 0);

            var iReturnCode = decimal.Parse(sResult[0].ToString());


            ContentDialog startSeggerApp = new ContentDialog();
            ContentDialogResult startSeggerAppResult;
            startSeggerApp.Title = "Segger Erase and Program Startup";
            startSeggerApp.Content = "Completed Code = " + iReturnCode.ToString();
            startSeggerApp.CloseButtonText = "OK";

            startSeggerAppResult = await startSeggerApp.ShowAsync();

            RunEraseProgram.IsEnabled = true;
        }

        private async void RunEraseProgramStart_Click(object sender, RoutedEventArgs e)
        {
            string sResult = "";

            RunEraseProgramStart.IsEnabled = false;

            Clipboard.Clear();

            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("JFlasherEraseProgramStartGroup");

            do
            {
                await Task.Delay(1000);
                sResult = await FetchClipboard();
            }
            while (String.Compare(sResult, "", true) == 0);

            var iReturnCode = decimal.Parse(sResult[0].ToString());

            ContentDialog startSeggerApp = new ContentDialog();
            ContentDialogResult startSeggerAppResult;
            startSeggerApp.Title = "Segger Erase, Program, and Startup";
            startSeggerApp.Content = "Completed Code = " + iReturnCode.ToString();
            startSeggerApp.CloseButtonText = "OK";

            startSeggerAppResult = await startSeggerApp.ShowAsync();

            RunEraseProgramStart.IsEnabled = true;
        }

        private async void RunStart_Click(object sender, RoutedEventArgs e)
        {
            string sResult = "";

            RunStart.IsEnabled = false;

            Clipboard.Clear();

            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("JFlasherStartGroup");

            do
            {
                await Task.Delay(1000);
                sResult = await FetchClipboard();
            }
            while (String.Compare(sResult, "", true) == 0);

            var iReturnCode = decimal.Parse(sResult[0].ToString());

            ContentDialog startSeggerApp = new ContentDialog();
            ContentDialogResult startSeggerAppResult;
            startSeggerApp.Title = "Segger Startup";
            startSeggerApp.Content = "Completed Code = " + iReturnCode.ToString();
            startSeggerApp.CloseButtonText = "OK";

            startSeggerAppResult = await startSeggerApp.ShowAsync();

            RunStart.IsEnabled = true;
        }

        private static async Task<string> FetchClipboard()
        {
            DataPackageView dpv = Clipboard.GetContent();

            if(dpv.Contains(StandardDataFormats.Text))
            {
                string text = await dpv.GetTextAsync();
                return text;
            }
            return "";
        }

        private async void RunZebra_Click(object sender, RoutedEventArgs e)
        {
            string sResult = "";

            RunZebra.IsEnabled = false;

            Clipboard.Clear();

            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("ZebraStartupGroup");

            do
            {
                await Task.Delay(1000);
                sResult = await FetchClipboard();
            }
            while (String.Compare(sResult, "", true) == 0);

            var iReturnCode = decimal.Parse(sResult[0].ToString());

            ContentDialog startSeggerApp = new ContentDialog();
            ContentDialogResult startSeggerAppResult;
            startSeggerApp.Title = "Zebra Startup";
            startSeggerApp.Content = "Completed Code = " + iReturnCode.ToString();
            startSeggerApp.CloseButtonText = "OK";

            startSeggerAppResult = await startSeggerApp.ShowAsync();

            RunZebra.IsEnabled = true;
        }

        public async static Task MakeStorageFolder()
        {
            FolderPicker folderPicker = new FolderPicker();
            StorageFolder folder = await folderPicker.PickSingleFolderAsync();
            if (folder != null)
            {
                StorageApplicationPermissions.FutureAccessList.AddOrReplace("PickedFolderToken", folder);
            }
            StorageFolder newFolder;
            // Creates the folder from the results of the Picker. Works. 
            newFolder = await folder.CreateFolderAsync("TestFolder");
            // Fails. Same folder, create from path with an added file name.
            newFolder = await StorageFolder.GetFolderFromPathAsync(Path.Combine(folder.Path, "TestFolder").ToString());
        }

        public async static Task<StorageFile> TestFolderSelection()
        {
            StorageFile filefound = null;
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".db");
            IReadOnlyList<StorageFile> files = await openPicker.PickMultipleFilesAsync();
            if (files.Count > 0)
            {
                // Application now has read/write access to the picked file(s)
                foreach (StorageFile file2 in files)
                    filefound = file2;
            }

            return filefound;
        }

        private async void SelectFile_Click(object sender, RoutedEventArgs e)
        {
            FolderPicker openPicker = new FolderPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".hex");

            StorageFolder sf = await openPicker.PickSingleFolderAsync();

            if(sf != null)
            {
                IReadOnlyList<StorageFile> files = await sf.GetFilesAsync();
                List<StorageFile> hexfiles = new List<StorageFile>();
                if (files.Count > 0)
                {
                    // Application now has read/write access to the picked file(s)
                    foreach (StorageFile file2 in files)
                    {
                        if (String.Compare(file2.FileType, ".hex", true) == 0)
                            hexfiles.Add(file2);
                    }
                }
            }

            var fal = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList;
            string mrutoken = fal.Add(sf, "firmwarelist");
            StorageFolder sf2 = await fal.GetFolderAsync(mrutoken);

            //Display a list of the hex files - minus the CurrentFiirmware.hex file

            //Let user pick the file they want.
            //Find in the stored file list

            return;

            /*
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.ComputerFolder;
            openPicker.FileTypeFilter.Add(".hex");

            StorageFile file = await openPicker.PickSingleFileAsync();

            if (file != null)
            {
                //StorageFolder parentFolder = await KnownFolders.GetFolderForUserAsync(null, KnownFolderId.DocumentsLibrary);

                
                if(parentFolder != null)
                {
                    await file.CopyAsync(parentFolder, "CurrentFirmware2.hex");
                }
                
                //StorageFolder sf = await StorageFolder.GetFolderFromPathAsync(file.Path);
                //StorageFolder sf = await file.OpenTransactedWriteAsync();
                //var fal = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList;
                //string mrutoken = fal.Add(file, "firmare");
                //StorageFolder sf = await fal.GetItemAsync(mrutoken);
                //await file.CopyAsync(sf, "NewName.hex");
                //await file.RenameAsync(file.Name, NameCollisionOption.GenerateUniqueName);
            }

            return;

            IReadOnlyList<StorageFile> files = await openPicker.PickMultipleFilesAsync();
            if (files.Count > 0)
            {
                StringBuilder output = new StringBuilder("Picked files:\n");
                // Application now has read/write access to the picked file(s)
                foreach (StorageFile file2 in files)
                {
                    output.Append(file2.Name + "\n");
                }

            }
            else
            {

            }

            
*/


            /*
            FolderPicker folderPicker = new FolderPicker();

            folderPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;

            folderPicker.FileTypeFilter.Add(".db");

            StorageFolder folder = await folderPicker.PickSingleFolderAsync();

            if (folder != null)
            {
                StorageApplicationPermissions.FutureAccessList.AddOrReplace("PickedFolderToken", folder);
            }
            StorageFolder newFolder;

            // Creates the folder from the results of the Picker. Works. 
            newFolder = await folder.CreateFolderAsync("TestFolder");

            return;
            */

            /*
            // Clear previous returned file name, if it exists, between iterations of this scenario
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".db");

            StorageFile file = await openPicker.PickSingleFileAsync();

            if (file != null)

            {

                // Application now has read/write access to the picked file

  

            }

            else

            {


            }

    */
        }

        private async void TestFile_Click(object sender, RoutedEventArgs e)
        {
            var fal = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList;
            string mrutoken = "{2D402A29-9741-42E4-961B-C868032217BA}";
            StorageFolder sf2 = await fal.GetFolderAsync(mrutoken);
        }
    }
}
