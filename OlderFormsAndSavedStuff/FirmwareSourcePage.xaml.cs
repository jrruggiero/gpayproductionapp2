﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using GPayProductionApp.Models;
using GPayProductionApp2.Services;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

using Microsoft.Toolkit.Uwp.UI.Controls;

namespace GPayProductionApp2.Views
{
    public sealed partial class FirmwareSourcePage : Page, INotifyPropertyChanged
    {
        private FirmwareSource _selected;

        public FirmwareSource Selected
        {
            get { return _selected; }
            set { Set(ref _selected, value); }
        }

        public ObservableCollection<FirmwareSource> FirmwareSource { get; private set; } = new ObservableCollection<FirmwareSource>();


        // TODO WTS: Change the grid as appropriate to your app, adjust the column definitions on TelerikDataGrid1Page.xaml.
        // For help see http://docs.telerik.com/windows-universal/controls/raddatagrid/gettingstarted
        // You may also want to extend the grid to work with the RadDataForm http://docs.telerik.com/windows-universal/controls/raddataform/dataform-gettingstarted
        public FirmwareSourcePage()
        {
            InitializeComponent();

            Loaded += FirmwareSourcePage_Loaded;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Initialize();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {

        }

        private void App_Resuming(object sender, object e)
        {

        }

        private async void FirmwareSourcePage_Loaded(object sender, RoutedEventArgs e)
        {
          
            FirmwareSource.Clear();

            var data = await SampleDataService.GetAllFirmwareSource();

            foreach (var item in data)
            {
                FirmwareSource.Add(item);
            }
            

            /*
            if (MasterDetailsViewControl.ViewState == MasterDetailsViewState.Both)
            {
                Selected = SampleItems.First();
            }
            */
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
