﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Automation.Peers;
using Windows.UI.Xaml.Media;

//using SDKTemplate;
//using System;
using Windows.Security.Credentials.UI;
using System.Reflection;

using System.Threading.Tasks;

using System.Collections.ObjectModel;

namespace GPayProductionApp2.Views
{
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        public static MainPage Current;
        public static bool blLoggedIn = false;

        public String[] ApplicationNameFull;
        public String ApplicationName;

        public MainPage()
        {
            InitializeComponent();

            var d = typeof(App).GetTypeInfo().Assembly.FullName;
            ApplicationNameFull = d.ToString().Split(',');
            ApplicationName = ApplicationNameFull[0];

            // This is a static public property that allows downstream pages to get a handle to the MainPage instance
            // in order to call methods that are in this class.
            Current = this;
            //SampleTitle.Text = FEATURE_NAME;

            Loaded += MainPage_Loaded;
        }

        private async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!blLoggedIn)
            {
                bool blResult = await LoginOperation();

                if (!blResult)
                    Application.Current.Exit();
            }

            GetAllCredentialsFromLocker();
        }

        /// <summary>
        /// Display a message to the user.
        /// This method may be called from any thread.
        /// </summary>
        /// <param name="strMessage"></param>
        /// <param name="type"></param>
        public void NotifyUser(string strMessage, NotifyType type)
        {
            // If called from the UI thread, then update immediately.
            // Otherwise, schedule a task on the UI thread to perform the update.
            if (Dispatcher.HasThreadAccess)
            {
                UpdateStatus(strMessage, type);
            }
            else
            {
                var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UpdateStatus(strMessage, type));
            }
        }

        public enum NotifyType
        {
            StatusMessage,
            ErrorMessage
        };

        private void UpdateStatus(string strMessage, NotifyType type)
        {
            switch (type)
            {
                case NotifyType.StatusMessage:
                    //StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                    break;
                case NotifyType.ErrorMessage:
                    //StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Red);
                    break;
            }

            StatusBlock.Text = strMessage;

            // Collapse the StatusBlock if it has no text to conserve real estate.
            //StatusBorder.Visibility = (StatusBlock.Text != String.Empty) ? Visibility.Visible : Visibility.Collapsed;
            if (StatusBlock.Text != String.Empty)
            {
                //StatusBorder.Visibility = Visibility.Visible;
                StatusPanel.Visibility = Visibility.Visible;
            }
            else
            {
                //StatusBorder.Visibility = Visibility.Collapsed;
                StatusPanel.Visibility = Visibility.Collapsed;
            }

            // Raise an event if necessary to enable a screen reader to announce the status update.
            var peer = FrameworkElementAutomationPeer.FromElement(StatusBlock);
            if (peer != null)
            {
                peer.RaiseAutomationEvent(AutomationEvents.LiveRegionChanged);
            }
        }

         protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Initialize();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {

        }

        private void App_Resuming(object sender, object e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private bool LoginAction(CredentialPickerResults credpickres)
        {

            NotifyUser("Hello test message", NotifyType.StatusMessage);

            var loginCredential = GetCredentialFromLocker(credpickres.CredentialUserName);

            if (loginCredential != null)
            {
                // There is a credential stored in the locker.
                // Populate the Password property of the credential
                // for automatic login.
                loginCredential.RetrievePassword();
            }
            else
            {
                // There is no credential stored in the locker.
                // Display UI to get user credentials.
                //loginCredential = GetLoginCredentialUI();
            }

            if ((string.Compare(loginCredential.UserName, credpickres.CredentialUserName) == 0)
                && (string.Compare(loginCredential.Password, credpickres.CredentialPassword) == 0))
                return true;

            // Log the user in.
            // ServerLogin(loginCredential.UserName, loginCredential.Password);

            return false;
        }

        private bool AddCredentials(string userName, string passWord)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            vault.Add(new Windows.Security.Credentials.PasswordCredential(ApplicationName, userName, passWord));

            return true;
        }

        private bool RemoveCredentials(string userName, string passWord)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            vault.Remove(new Windows.Security.Credentials.PasswordCredential(ApplicationName, userName, passWord));

            return true;
        }

        private bool ExistCredentials(string username)
        {
            var loginCredential = GetCredentialFromLocker(username);

            if (loginCredential != null)
                return true;

            return false;
        }

        private Windows.Security.Credentials.PasswordCredential GetCredentialFromLocker(string sUserNameForCredential)
        {
            Windows.Security.Credentials.PasswordCredential credential = null;

            var vault = new Windows.Security.Credentials.PasswordVault();
            var credentialList = vault.FindAllByResource(ApplicationName);
            if (credentialList.Count > 0)
            {
                if (credentialList.Count == 1)
                {
                    credential = credentialList[0];
                }
                else
                {
                    try
                    {
                        credential = vault.Retrieve(ApplicationName, sUserNameForCredential);
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }

            return credential;
        }

        public ObservableCollection<string> CredentialListStrings { get; private set; } = new ObservableCollection<string>();

        private ObservableCollection<string> GetAllCredentialsFromLocker()
        {
            CredentialListStrings.Clear();

            var vault = new Windows.Security.Credentials.PasswordVault();
            var credentialList = vault.FindAllByResource(ApplicationName);

            foreach(var item in credentialList)
            {
                CredentialListStrings.Add(item.UserName);
            }
   
            return CredentialListStrings;
        }

        private async Task<bool> LoginOperation()
        {
            if (blLoggedIn)
                return true;

            try
            {
                CredentialPickerOptions credPickerOptions = new CredentialPickerOptions();
                credPickerOptions.Message = "Please enter your username and password";
                credPickerOptions.Caption = "Login";
                credPickerOptions.TargetName = ApplicationName;
                credPickerOptions.AlwaysDisplayDialog = true;
                credPickerOptions.CallerSavesCredential = false;
                credPickerOptions.CredentialSaveOption = CredentialSaveOption.Hidden;
                credPickerOptions.AuthenticationProtocol = AuthenticationProtocol.Basic;

                CredentialPickerResults credPickerResults = await Windows.Security.Credentials.UI.CredentialPicker.PickAsync(credPickerOptions);

                if (credPickerResults.ErrorCode != 0)
                    return false;

                //SetResult(credPickerResults);

                if (!ExistCredentials(credPickerResults.CredentialUserName))
                {
                    //Username and or password is incorrect
                    ContentDialog noUserPwd = new ContentDialog();
                    ContentDialogResult noUserResult;
                    noUserPwd.Title = "Entry incorrect";
                    noUserPwd.Content = "User does not exist";
                    noUserPwd.CloseButtonText = "OK";

                    noUserResult = await noUserPwd.ShowAsync();

                    return false;
                }

                if (!LoginAction(credPickerResults))
                {
                    //Username and or password is incorrect
                    ContentDialog noUserPwd = new ContentDialog();
                    ContentDialogResult noUserResult;
                    noUserPwd.Title = "Entry incorrect";
                    noUserPwd.Content = "Password is incorrect";
                    noUserPwd.CloseButtonText = "OK";

                    noUserResult = await noUserPwd.ShowAsync();
                    //if(noUserResult.)
                }

                blLoggedIn = true;
                return true;
            }
            catch (Exception ex)
            {
                //SetError(ex.GetType().ToString() + ": " + ex.Message);
                return false;
            }
        }

        private async void AddUser_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CredentialPickerOptions credPickerOptions = new CredentialPickerOptions();
                credPickerOptions.Message = "Please enter a new username and password";
                credPickerOptions.Caption = "New User";
                credPickerOptions.TargetName = ApplicationName;
                credPickerOptions.AlwaysDisplayDialog = true;
                credPickerOptions.CallerSavesCredential = false;
                credPickerOptions.CredentialSaveOption = CredentialSaveOption.Hidden;
                credPickerOptions.AuthenticationProtocol = AuthenticationProtocol.Basic;

                CredentialPickerResults credPickerResults = await Windows.Security.Credentials.UI.CredentialPicker.PickAsync(credPickerOptions);
                //SetResult(credPickerResults);

                if (!AddCredentials(credPickerResults.CredentialUserName, credPickerResults.CredentialPassword))
                {
                    //Username and or password is incorrect
                    ContentDialog noUserPwd = new ContentDialog();
                    noUserPwd.Title = "Entry incorrect";
                    noUserPwd.Content = "User already exists";
                    noUserPwd.CloseButtonText = "OK";

                    await noUserPwd.ShowAsync();

                    return;
                }

                //CredentialListStrings.CollectionChanged(sender);

                GetAllCredentialsFromLocker();
            }
            catch (Exception ex)
            {
                //SetError(ex.GetType().ToString() + ": " + ex.Message);
            }

        }

        private async void RemoveUser_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CredentialPickerOptions credPickerOptions = new CredentialPickerOptions();
                credPickerOptions.Message = "Please enter remove a username and password";
                credPickerOptions.Caption = "Remove User";
                credPickerOptions.TargetName = ApplicationName;
                credPickerOptions.AlwaysDisplayDialog = true;
                credPickerOptions.CallerSavesCredential = false;
                credPickerOptions.CredentialSaveOption = CredentialSaveOption.Hidden;
                credPickerOptions.AuthenticationProtocol = AuthenticationProtocol.Basic;

                CredentialPickerResults credPickerResults = await Windows.Security.Credentials.UI.CredentialPicker.PickAsync(credPickerOptions);
                //SetResult(credPickerResults);

                if (!RemoveCredentials(credPickerResults.CredentialUserName, credPickerResults.CredentialPassword))
                {
                    //Username and or password is incorrect
                    ContentDialog noUserPwd = new ContentDialog();
                    noUserPwd.Title = "Entry incorrect";
                    noUserPwd.Content = "User already exists";
                    noUserPwd.CloseButtonText = "OK";

                    await noUserPwd.ShowAsync();

                    return;
                }

                GetAllCredentialsFromLocker();
            }
            catch (Exception ex)
            {
                //SetError(ex.GetType().ToString() + ": " + ex.Message);
            }

        }

        /*
        private void Data_Loaded(object sender, RoutedEventArgs e)
        {
            var listBox = (ListBox)sender;
            listBox.ItemsSource = GetAllCredentialsFromLocker();

        }
        */
    }


}
