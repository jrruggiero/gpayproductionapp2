﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using GPayProductionApp.Models;
using GPayProductionApp2.Services;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

using Microsoft.Toolkit.Uwp.UI.Controls;

using System.Collections.Generic;
using Windows.Storage.Streams;


using Windows.Devices.Bluetooth.Advertisement;

//using SDKTemplate;
using Windows.ApplicationModel.DataTransfer;

namespace GPayProductionApp2.Views
{
    public sealed partial class LabelInterimResultPage : Page, INotifyPropertyChanged
    {
        // The Bluetooth LE advertisement watcher class is used to control and customize Bluetooth LE scanning.
        private BluetoothLEAdvertisementWatcher watcher;
        private DataPackage dataPackage = new DataPackage();
        private DateTime dt;
        private LabelInterimResult _selected;

        public LabelInterimResult Selected
        {
            get { return _selected; }
            set { Set(ref _selected, value); }
        }

        public ObservableCollection<LabelInterimResult> LabelInterimResult { get; private set; } = new ObservableCollection<LabelInterimResult>();

        // TODO WTS: Change the grid as appropriate to your app, adjust the column definitions on TelerikDataGrid2Page.xaml.
        // For help see http://docs.telerik.com/windows-universal/controls/raddatagrid/gettingstarted
        // You may also want to extend the grid to work with the RadDataForm http://docs.telerik.com/windows-universal/controls/raddataform/dataform-gettingstarted
        public LabelInterimResultPage()
        {
            InitializeComponent();
            Loaded += LabelInterimResult_Loaded;
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Initialize();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {

        }

        private void App_Resuming(object sender, object e)
        {

        }

        private async void LabelInterimResult_Loaded(object sender, RoutedEventArgs e)
        {
            LabelInterimResult.Clear();

            var data = await SampleDataService.GetLabelInterimResult();

            foreach (var item in data)
            {
                LabelInterimResult.Add(item);
            }

            /*
            if (MasterDetailsViewControl.ViewState == MasterDetailsViewState.Both)
            {
                Selected = SampleItems.First();

                            //var data = await SampleDataService.UpdateLabelInterimResult(_selected);
            }
            */
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private async void LabelInterimResult_SelectionChanged(object sender, Telerik.UI.Xaml.Controls.Grid.DataGridSelectionChangedEventArgs e)
        {
            dt = DateTime.Now;

            _selected.PartNumberRevision = "C";

            String year = dt.Year.ToString();
            year = year.Substring(2);

            String strMac = string.Format("{0},{1}:{2}:{3}:{4}:{5}",
                                           "FF", "FF", "FF", "FF", "FF", "FF");

            _selected.PartNumber = "A006406-1";
            _selected.PartNumberRevision = "B";
            _selected.DateCode = year + dt.DayOfYear.ToString();
            _selected.SerialNumberMACAddress = strMac;

            var data = await SampleDataService.UpdateLabelInterimResult(_selected);
        }

        private void LabelContent()
        {
            /*
            // We can obtain various information about the advertisement we just received by accessing 
            // the properties of the EventArgs class

            // The timestamp of the event
            DateTimeOffset timestamp = eventArgs.Timestamp;

            // The type of advertisement
            BluetoothLEAdvertisementType advertisementType = eventArgs.AdvertisementType;

            // The received signal strength indicator (RSSI)
            Int16 rssi = eventArgs.RawSignalStrengthInDBm;

            // The local name of the advertising device contained within the payload, if any
            string localName = eventArgs.Advertisement.LocalName;

            // Check if there are any manufacturer-specific sections.
            // If there is, print the raw data of the first manufacturer section (if there are multiple).
            string manufacturerDataString = "";
            var manufacturerSections = eventArgs.Advertisement.ManufacturerData;
            if (manufacturerSections.Count > 0)
            {
                // Only print the first one of the list
                var manufacturerData = manufacturerSections[0];
                var data = new byte[manufacturerData.Data.Length];
                var macdata = new byte[6];
                using (var reader = DataReader.FromBuffer(manufacturerData.Data))
                {
                    reader.ReadBytes(data);
                }
                // Print the company ID + the raw data in hex format
                //manufacturerDataString = string.Format("0x{0}: {1}",
                //    manufacturerData.CompanyId.ToString("X"),
                //    BitConverter.ToString(data));

                Array.Reverse(data);
                Array.Copy(data, macdata, 6);

                manufacturerDataString = string.Format("0x{0}: MAC: {1}",
                    manufacturerData.CompanyId.ToString("X"),
                    BitConverter.ToString(macdata));

                String strMac = string.Format("A006406-1,B,{0},{1}:{2}:{3}:{4}:{5}:{6}",
                                                "18" + dt.DayOfYear.ToString(),
                                                macdata[0].ToString("X02"),
                                                macdata[1].ToString("X02"),
                                                macdata[2].ToString("X02"),
                                                macdata[3].ToString("X02"),
                                                macdata[4].ToString("X02"),
                                                macdata[5].ToString("X02"));

                dataPackage.RequestedOperation = DataPackageOperation.Copy;
                dataPackage.SetText(strMac);

                // Stopping the watcher will stop scanning if this is the only client requesting scan
                watcher.Stop();

                rootPage.NotifyUser("Watcher stopped.", NotifyType.StatusMessage);
                */

            }
        }
}
