﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

using GPayProductionApp.Models;
using GPayProductionApp2.Services;

using Microsoft.Toolkit.Uwp.UI.Controls;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace GPayProductionApp2.Views
{
    public sealed partial class SalesOrderDetailPage2 : Page, INotifyPropertyChanged
    {
        private SalesOrders _selected;

        public SalesOrders Selected
        {
            get { return _selected; }
            set { Set(ref _selected, value); }
        }

        public ObservableCollection<SalesOrders> SalesOrders { get; private set; } = new ObservableCollection<SalesOrders>();

        public SalesOrderDetailPage2()
        {
            InitializeComponent();
            Loaded += SalesOrderDetailPage2_Loaded;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Initialize();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {

        }

        private void App_Resuming(object sender, object e)
        {

        }

        private async void SalesOrderDetailPage2_Loaded(object sender, RoutedEventArgs e)
        {
            SalesOrders.Clear();

            var data = await SampleDataService.GetAllSalesOrders();

            foreach (var item in data)
            {
                SalesOrders.Add(item);
            }

            if (MasterDetailsViewControl.ViewState == MasterDetailsViewState.Both)
            {
                Selected = SalesOrders.First();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
