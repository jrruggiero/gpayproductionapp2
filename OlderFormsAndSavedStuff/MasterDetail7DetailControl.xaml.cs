﻿using System;

using GPayProductionApp.Models;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace GPayProductionApp2.Views
{
    public sealed partial class MasterDetail7DetailControl : UserControl
    {
        public SampleOrder MasterMenuItem
        {
            get { return GetValue(MasterMenuItemProperty) as SampleOrder; }
            set { SetValue(MasterMenuItemProperty, value); }
        }

        public static readonly DependencyProperty MasterMenuItemProperty = DependencyProperty.Register("MasterMenuItem", typeof(SampleOrder), typeof(MasterDetail7DetailControl), new PropertyMetadata(null, OnMasterMenuItemPropertyChanged));

        public MasterDetail7DetailControl()
        {
            InitializeComponent();
        }

        private static void OnMasterMenuItemPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var control = d as MasterDetail7DetailControl;
            control.ForegroundElement.ChangeView(0, 0, 1);
        }
    }
}
