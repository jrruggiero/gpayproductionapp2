﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using GPayProductionApp.Models;
using GPayProductionApp2.Services;

using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Telerik.Data.Core;
using System.Windows.Input;

namespace GPayProductionApp2.Views
{
    public sealed partial class TelerikDataGrid3Page : Page, INotifyPropertyChanged
    {
        private SampleOrder _selected;

        public ObservableCollection<SampleOrder> SampleOrders { get; private set; } = new ObservableCollection<SampleOrder>();

        public SampleOrder SelectedOrder
        {
            get { return _selected; }
            set { Set(ref _selected, value); }
        }

        // TODO WTS: Change the grid as appropriate to your app, adjust the column definitions on TelerikDataGrid3Page.xaml.
        // For help see http://docs.telerik.com/windows-universal/controls/raddatagrid/gettingstarted
        // You may also want to extend the grid to work with the RadDataForm http://docs.telerik.com/windows-universal/controls/raddataform/dataform-gettingstarted
        public TelerikDataGrid3Page()
        {
            InitializeComponent();

            //dataForm.Item = _contact;
            //Loaded += SalesOrderDetailPage2_Loaded;

            SampleOrder datum = null;

            SampleOrders.Clear();

            var data = SampleDataService.GetGridSampleData();

            foreach (var item in data)
            {
                if (datum == null)
                    datum = item;

                SampleOrders.Add(item);
            }

            //if (MasterDetailsViewControl.ViewState == MasterDetailsViewState.Both)
            //{
            SelectedOrder = datum;
            //}

            // create a DeleteCommand instance
            DeleteCommand = new DelegateCommand(ExecuteDeleteCommand);
            NewCommand = new DelegateCommand(ExecuteNewCommand);
            DataContext = this;
        }

        // This property will be bound to button's Command property for deleting item
        public ICommand DeleteCommand { set; get; }
        public ICommand NewCommand { set; get; }


        private void ExecuteDeleteCommand(object param)
        {

            int id = (Int32)param;
            /*
            Customer cus = GetCustomerById(id);

            if (cus != null)
            {
                Customers.Remove(cus);
            }*/

        }

        void ExecuteNewCommand(object param)
        {

            int id = (Int32)param;
            /*
            Customer cus = GetCustomerById(id);

            if (cus != null)
            {
                Customers.Remove(cus);
            }*/

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Initialize();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {

        }

        private void App_Resuming(object sender, object e)
        {

        }

        public ObservableCollection<SampleOrder> Source
        {
            get
            {
                // TODO WTS: Replace this with your actual data
                return SampleDataService.GetGridSampleData();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public class Contact
    {
        private string email;
        private string firstName;
        private string lastName;
        private UserTitle title;
        private string company;
        private string phonenumber;

        public enum UserTitle
        {
            Mr,
            Mrs,
            Miss
        }


        public string Email
        {
            get
            {
                return this.email;
            }
            set
            {
                this.email = value;
            }
        }

        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                this.firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
            }
        }

        public UserTitle Title
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        public string Company
        {
            get
            {
                return this.company;
            }
            set
            {
                this.company = value;
            }
        }

        public string PhoneNumber
        {
            get
            {
                return phonenumber;
            }
            set
            {
                phonenumber = value;
            }
        }

    }
}
