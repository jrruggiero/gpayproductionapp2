﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPayProductionApp.Models
{
    public interface ISalesOrdersRepository
    {
        /// <summary>
        /// Returns all firmware sources. 
        /// </summary>
        Task<IEnumerable<SalesOrders>> GetAsync();

        /// <summary>
        /// Returns the firmware sources the given id.
        /// </summary>
        Task<SalesOrders> GetAsync(long id);

        /// <summary>
        /// Returns all firmware sources with a data field matching the start of the given string. 
        /// </summary>
        Task<IEnumerable<SalesOrders>> GetAsync(string search);

        /// <summary>
        /// Returns all the given customer's orders. 
        /// </summary>
        //Task<IEnumerable<FirmwareSource>> GetForCustomerAsync(Guid customerId);

        /// <summary>
        /// Adds a new firmware source if it doesn't exist updates the 
        /// existing order otherwise.
        /// </summary>
        Task<SalesOrders> UpsertAsync(SalesOrders record);

        /// <summary>
        /// Deletes an order.
        /// </summary>
        Task<SalesOrders> DeleteAsync(long id);

        /// <summary>
        /// Deletes an order.
        /// </summary>
        Task<SalesOrders> DeleteAsync(SalesOrders record);
    }
}
