﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GPayProductionApp.Models;

namespace GPayProductionApp.Repository
{
    public interface IGPayProductionAppRepository
    {
        IFirmwareSourceRepository FirmwareSource { get; }

        IFinalResultsRepository FinalResults { get; }

        ILabelInterimResultRepository LabelInterimResult { get; }

        IProgrammerResultsRepository ProgrammerResults { get; }

        ISalesOrdersRepository SalesOrders { get; }

        ITestResultsRepository TestResults { get; }
    }
}
