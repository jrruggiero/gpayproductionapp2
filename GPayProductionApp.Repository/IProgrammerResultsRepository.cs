﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GPayProductionApp.Models
{
    public interface IProgrammerResultsRepository
    {
        /// <summary>
        /// Returns all firmware sources. 
        /// </summary>
        Task<IEnumerable<ProgrammerResults>> GetAsync();

        /// <summary>
        /// Returns the firmware sources the given id.
        /// </summary>
        Task<ProgrammerResults> GetAsync(long firmwaresourceId);

        /// <summary>
        /// Returns all firmware sources with a data field matching the start of the given string. 
        /// </summary>
        Task<IEnumerable<ProgrammerResults>> GetAsync(string search);

        /// <summary>
        /// Returns all the given customer's orders. 
        /// </summary>
        //Task<IEnumerable<FirmwareSource>> GetForCustomerAsync(Guid customerId);

        /// <summary>
        /// Adds a new firmware source if it doesn't exist updates the 
        /// existing order otherwise.
        /// </summary>
        Task<ProgrammerResults> UpsertAsync(ProgrammerResults soucerecord);

        /// <summary>
        /// Deletes an order.
        /// </summary>
        Task<ProgrammerResults> DeleteAsync(long firmwaresourceID);
    }
}
