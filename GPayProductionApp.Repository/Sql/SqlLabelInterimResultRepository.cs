﻿using GPayProductionApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GPayProductionApp.Repository.Sql
{
    /// <summary>
    /// Contains methods for interacting with the orders backend using 
    /// SQL via Entity Framework Core 2.0.
    /// </summary>
    public class SqlLabelInterimResultRepository : ILabelInterimResultRepository
    {
        private readonly GPayProductionAppContext _db; 

        public SqlLabelInterimResultRepository(GPayProductionAppContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<LabelInterimResult>> GetAsync()
        {
            return await _db.LabelInterimResult
                //.Include(x => x.LineItems)
                //.ThenInclude(x => x.Product)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<LabelInterimResult> GetAsync(long id)
        {
            return await _db.LabelInterimResult
                //.Include(x => x.LineItems)
                //.ThenInclude(x => x.Product)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<LabelInterimResult>> GetForCustomerAsync(long labelid)
        {
            return await _db.LabelInterimResult
                .Where(x => x.Id == labelid)
                //.Include(x => x.LineItems)
                //.ThenInclude(x => x.Product)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<IEnumerable<LabelInterimResult>> GetAsync(string value)
        {
            string[] parameters = value.Split(' ');
            return await _db.LabelInterimResult
                //.Include(x => x.Customer)
                //.Include(x => x.LineItems)
                //.ThenInclude(x => x.Product)
                /*
                .Where(x => parameters
                    .Any(y =>
                        x.Address.StartsWith(y) ||
                        x.Customer.FirstName.StartsWith(y) ||
                        x.Customer.LastName.StartsWith(y) ||
                        x.InvoiceNumber.ToString().StartsWith(y)))
                .OrderByDescending(x => parameters
                    .Count(y =>
                        x.Address.StartsWith(y) ||
                        x.Customer.FirstName.StartsWith(y) ||
                        x.Customer.LastName.StartsWith(y) ||
                        x.InvoiceNumber.ToString().StartsWith(y)))*/
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<LabelInterimResult> UpsertAsync(LabelInterimResult label)
        {
            var existing = await _db.LabelInterimResult.FirstOrDefaultAsync(x => x.Id == label.Id);
            if (null == existing)
            {
                //label.InvoiceNumber = _db.LabelInterimResult.Max(x => x.InvoiceNumber) + 1;
                _db.LabelInterimResult.Add(label);
            }
            else
            {
                _db.Entry(existing).CurrentValues.SetValues(label);
            }
            await _db.SaveChangesAsync();
            return label;
        }

        public async Task<LabelInterimResult> DeleteAsync(long labelid)
        {
            var match = await _db.LabelInterimResult.FindAsync(labelid);
            if (match != null)
            {
                _db.LabelInterimResult.Remove(match);
            }
            await _db.SaveChangesAsync();

            return match;
            /*
                        return await _db.LabelInterimResult
                        .AsNoTracking()
                        .FirstOrDefaultAsync(x => x.Id == labelid);
             */
        }

        public async Task<LabelInterimResult> DeleteAsync(LabelInterimResult resultProg)
        {

            var current = await _db.LabelInterimResult.FirstOrDefaultAsync(x => x.Id == resultProg.Id);
            if (current != null)
            {
                //var orders = await _db.Orders.Where(x => x.CustomerId == id).ToListAsync();
                //_db.Orders.RemoveRange(orders);
                _db.LabelInterimResult.RemoveRange(current);
            }

            await _db.SaveChangesAsync();

            return resultProg;
        }
    }
}
