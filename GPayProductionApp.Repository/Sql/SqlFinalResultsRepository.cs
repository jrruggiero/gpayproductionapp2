﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GPayProductionApp.Models;

namespace GPayProductionApp.Repository.Sql
{
    /// <summary>
    /// Contains methods for interacting with the customers backend using 
    /// SQL via Entity Framework Core 2.0.
    /// </summary>
    public class SqlFinalResultsRepository : IFinalResultsRepository
    {
        private readonly GPayProductionAppContext _db; 

        public SqlFinalResultsRepository(GPayProductionAppContext db)
        {
            _db = db; 
        }

        public async Task<IEnumerable<FinalResults>> GetAsync()
        {
            return await _db.FinalResults
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<FinalResults> GetAsync(long id)
        {
            return await _db.FinalResults
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<FinalResults>> GetAsync(string value)
        {
            string[] parameters = value.Split(' ');
            return await _db.FinalResults
                /*.Where(x =>
                    parameters.Any(y =>
                        x.FirstName.StartsWith(y) ||
                        x.LastName.StartsWith(y) ||
                        x.Email.StartsWith(y) ||
                        x.Phone.StartsWith(y) ||
                        x.Address.StartsWith(y)))
                .OrderByDescending(x =>
                    parameters.Count(y =>
                        x.FirstName.StartsWith(y) ||
                        x.LastName.StartsWith(y) ||
                        x.Email.StartsWith(y) ||
                        x.Phone.StartsWith(y) ||
                        x.Address.StartsWith(y)))*/
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<FinalResults> UpsertAsync(FinalResults result)
        {
            var current = await _db.FinalResults.FirstOrDefaultAsync(x => x.Id == result.Id);
            if (null == current)
            {
                _db.FinalResults.Add(result);
            }
            else
            {
                _db.Entry(current).CurrentValues.SetValues(result);
            }
            await _db.SaveChangesAsync();
            return result;
        }

        public async Task<FinalResults> DeleteAsync(long id)
        {
            /*
            var result = await _db.FinalResults.FirstOrDefaultAsync(x => x.Id == id);
            if (null != result)
            {
                //var orders = await _db.Orders.Where(x => x.CustomerId == id).ToListAsync();
                //_db.Orders.RemoveRange(orders);
                _db.FinalResults.Remove(result);
                await _db.SaveChangesAsync();
            }
            */

            return await _db.FinalResults
                        .AsNoTracking()
                        .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
