﻿using GPayProductionApp.Models;
using Microsoft.EntityFrameworkCore;

namespace GPayProductionApp.Repository.Sql
{
    /// <summary>
    /// Entity Framework Core DbContext for Contoso.
    /// </summary>
    public class GPayProductionAppContext : DbContext
    {
        /// <summary>
        /// Creates a new Contoso DbContext.
        /// </summary>
        public GPayProductionAppContext(DbContextOptions<GPayProductionAppContext> options) : base(options)
        { }

        public DbSet<FinalResults> FinalResults { get; set; }

        public DbSet<FirmwareSource> FirmwareSource { get; set; }

        public DbSet<LabelInterimResult> LabelInterimResult { get; set; }

        public DbSet<ProgrammerResults> ProgrammerResults { get; set; }

        public DbSet<SalesOrders> SalesOrders { get; set; }

        public DbSet<TestResults> TestResults { get; set; }

        /// <summary>
        /// Gets the line items DbSet.
        /// </summary>
        //public DbSet<LineItem> LineItems { get; set; }
    }
}
