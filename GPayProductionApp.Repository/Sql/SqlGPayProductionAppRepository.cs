﻿using GPayProductionApp.Models;
using Microsoft.EntityFrameworkCore;

namespace GPayProductionApp.Repository.Sql
{
    /// <summary>
    /// Contains methods for interacting with the app backend using 
    /// SQL via Entity Framework Core 2.0. 
    /// </summary>
    public class SqlGPayProductionAppRepository : IGPayProductionAppRepository
    {
        private readonly DbContextOptions<GPayProductionAppContext> _dbOptions; 

        public SqlGPayProductionAppRepository(DbContextOptionsBuilder<GPayProductionAppContext> 
            dbOptionsBuilder)
        {
            _dbOptions = dbOptionsBuilder.Options;
            using (var db = new GPayProductionAppContext(_dbOptions))
            {
                db.Database.EnsureCreated(); 
            }
        }

        public IFirmwareSourceRepository FirmwareSource => new SqlFirmwareSourceRepository(
            new GPayProductionAppContext(_dbOptions));

        public IFinalResultsRepository FinalResults => new SqlFinalResultsRepository(
            new GPayProductionAppContext(_dbOptions));

        public ILabelInterimResultRepository LabelInterimResult => new SqlLabelInterimResultRepository(
            new GPayProductionAppContext(_dbOptions));

        public IProgrammerResultsRepository ProgrammerResults => new SqlProgrammerResultsRepository(
            new GPayProductionAppContext(_dbOptions));

        public ISalesOrdersRepository SalesOrders => new SqlSalesOrdersRepository(
            new GPayProductionAppContext(_dbOptions));

        public ITestResultsRepository TestResults => new SqlTestResultsRepository(
            new GPayProductionAppContext(_dbOptions));
    }
}
