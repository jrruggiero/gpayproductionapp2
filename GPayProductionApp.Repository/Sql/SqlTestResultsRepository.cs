﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GPayProductionApp.Models;

namespace GPayProductionApp.Repository.Sql
{
    /// <summary>
    /// Contains methods for interacting with the customers backend using 
    /// SQL via Entity Framework Core 2.0.
    /// </summary>
    public class SqlTestResultsRepository : ITestResultsRepository
    {
        private readonly GPayProductionAppContext _db;

        public SqlTestResultsRepository(GPayProductionAppContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<TestResults>> GetAsync()
        {
            return await _db.TestResults
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<TestResults> GetAsync(long id)
        {
            return await _db.TestResults
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<TestResults>> GetAsync(string value)
        {
            string[] parameters = value.Split(' ');
            return await _db.TestResults
                /*.Where(x =>
                    parameters.Any(y =>
                        x.FirstName.StartsWith(y) ||
                        x.LastName.StartsWith(y) ||
                        x.Email.StartsWith(y) ||
                        x.Phone.StartsWith(y) ||
                        x.Address.StartsWith(y)))
                .OrderByDescending(x =>
                    parameters.Count(y =>
                        x.FirstName.StartsWith(y) ||
                        x.LastName.StartsWith(y) ||
                        x.Email.StartsWith(y) ||
                        x.Phone.StartsWith(y) ||
                        x.Address.StartsWith(y)))*/
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<TestResults> UpsertAsync(TestResults resultprog)
        {
            var current = await _db.TestResults.FirstOrDefaultAsync(x => x.Id == resultprog.Id);
            if (null == current)
            {
                _db.TestResults.Add(resultprog);
            }
            else
            {
                _db.Entry(current).CurrentValues.SetValues(resultprog);
            }
            await _db.SaveChangesAsync();
            return resultprog;
        }

        public async Task<TestResults> DeleteAsync(long id)
        {
            /*
            var result = await _db.FinalResults.FirstOrDefaultAsync(x => x.Id == id);
            if (null != result)
            {
                //var orders = await _db.Orders.Where(x => x.CustomerId == id).ToListAsync();
                //_db.Orders.RemoveRange(orders);
                _db.FinalResults.Remove(result);
                await _db.SaveChangesAsync();
            }
            */

            return await _db.TestResults
                        .AsNoTracking()
                        .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
