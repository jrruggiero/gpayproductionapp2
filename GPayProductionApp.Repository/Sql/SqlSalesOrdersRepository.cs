﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GPayProductionApp.Models;

namespace GPayProductionApp.Repository.Sql
{
    /// <summary>
    /// Contains methods for interacting with the customers backend using 
    /// SQL via Entity Framework Core 2.0.
    /// </summary>
    public class SqlSalesOrdersRepository : ISalesOrdersRepository
    {
        private readonly GPayProductionAppContext _db;

        public SqlSalesOrdersRepository(GPayProductionAppContext db)
        {
            _db = db;
        }

        public async Task<IEnumerable<SalesOrders>> GetAsync()
        {
            return await _db.SalesOrders
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<SalesOrders> GetAsync(long id)
        {
            return await _db.SalesOrders
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<SalesOrders>> GetAsync(string value)
        {
            string[] parameters = value.Split(' ');
            return await _db.SalesOrders
                /*
                .Where(x =>
                    parameters.Any(y =>
                        x.FirstName.StartsWith(y) ||
                        x.LastName.StartsWith(y) ||
                        x.Email.StartsWith(y) ||
                        x.Phone.StartsWith(y) ||
                        x.Address.StartsWith(y)))
                .OrderByDescending(x =>
                    parameters.Count(y =>
                        x.FirstName.StartsWith(y) ||
                        x.LastName.StartsWith(y) ||
                        x.Email.StartsWith(y) ||
                        x.Phone.StartsWith(y) ||
                        x.Address.StartsWith(y)))
                        */
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<SalesOrders> UpsertAsync(SalesOrders resultprog)
        {
            var current = await _db.SalesOrders.FirstOrDefaultAsync(x => x.Id == resultprog.Id);
            if (null == current)
            {
                _db.SalesOrders.Add(resultprog);
            }
            else
            {
                _db.Entry(current).CurrentValues.SetValues(resultprog);
            }
            await _db.SaveChangesAsync();
            return resultprog;
        }

        public async Task<SalesOrders> DeleteAsync(long id)
        {
            /*
            var result = await _db.FinalResults.FirstOrDefaultAsync(x => x.Id == id);
            if (null != result)
            {
                //var orders = await _db.Orders.Where(x => x.CustomerId == id).ToListAsync();
                //_db.Orders.RemoveRange(orders);
                _db.FinalResults.Remove(result);
                await _db.SaveChangesAsync();
            }
            */

            return await _db.SalesOrders
                        .AsNoTracking()
                        .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<SalesOrders> DeleteAsync(SalesOrders resultprog)
        {
            /*
            var result = await _db.FinalResults.FirstOrDefaultAsync(x => x.Id == id);
            if (null != result)
            {
                //var orders = await _db.Orders.Where(x => x.CustomerId == id).ToListAsync();
                //_db.Orders.RemoveRange(orders);
                _db.FinalResults.Remove(result);
                await _db.SaveChangesAsync();
            }
            */

            //return await _db.SalesOrders
            //            .AsNoTracking()
            //            .FirstOrDefaultAsync(x => x.Id == id);

            var current = await _db.SalesOrders.FirstOrDefaultAsync(x => x.Id == resultprog.Id);
            if (current != null)
            {
                //var orders = await _db.Orders.Where(x => x.CustomerId == id).ToListAsync();
                //_db.Orders.RemoveRange(orders);
                _db.SalesOrders.RemoveRange(current);
            }

            await _db.SaveChangesAsync();

            return resultprog;
        }
    }
}
