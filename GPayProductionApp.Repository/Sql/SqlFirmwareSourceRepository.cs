﻿using GPayProductionApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GPayProductionApp.Repository.Sql
{
    /// <summary>
    /// Contains methods for interacting with the products backend using 
    /// SQL via Entity Framework Core 2.0.
    /// </summary>
    public class SqlFirmwareSourceRepository : IFirmwareSourceRepository
    {
        private readonly GPayProductionAppContext _db;

        public SqlFirmwareSourceRepository(GPayProductionAppContext db)
        {
            _db = db; 
        }

        public async Task<IEnumerable<FirmwareSource>> GetAsync()
        {
            return await _db.FirmwareSource
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<FirmwareSource> GetAsync(long id)
        {
            return await _db.FirmwareSource
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<FirmwareSource>> GetAsync(string valuePN, 
                                                                string valuePNRev,
                                                                int active)
        {
            return await _db.FirmwareSource
                .Where(x =>
                x.PartNumber.Equals(valuePN) 
                && x.Revision.Equals(valuePNRev)
                && x.Active.Equals(active))
            .AsNoTracking()
            .ToListAsync();
        }

        public async Task<FirmwareSource> UpsertAsync(FirmwareSource firmwaresource)
        {
            var existing = await _db.FirmwareSource.FirstOrDefaultAsync(x => x.Id == firmwaresource.Id);
            if (null == existing)
            {
                //firmwaresource.InvoiceNumber = _db.FirmwareSources.Max(x => x.InvoiceNumber) + 1;
                _db.FirmwareSource.Add(firmwaresource);
            }
            else
            {
                _db.Entry(existing).CurrentValues.SetValues(firmwaresource);
            }
            await _db.SaveChangesAsync();
            return firmwaresource;
        }

        public async Task<FirmwareSource> DeleteAsync(long id)
        {
            return await _db.FirmwareSource
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
