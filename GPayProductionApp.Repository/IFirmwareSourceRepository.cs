﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace GPayProductionApp.Models
{
    public interface IFirmwareSourceRepository
    {
        /// <summary>
        /// Returns all firmware sources. 
        /// </summary>
        Task<IEnumerable<FirmwareSource>> GetAsync();

        /// <summary>
        /// Returns the firmware sources the given id.
        /// </summary>
        Task<FirmwareSource> GetAsync(long firmwaresourceId);

        /// <summary>
        /// Returns all firmware sources with a data field matching the start of the given string. 
        /// </summary>
        Task<IEnumerable<FirmwareSource>> GetAsync(string searchPN, string searchPNRev, int iActive);

        /// <summary>
        /// Returns all the given customer's orders. 
        /// </summary>
        //Task<IEnumerable<FirmwareSource>> GetForCustomerAsync(Guid customerId);

        /// <summary>
        /// Adds a new firmware source if it doesn't exist updates the 
        /// existing order otherwise.
        /// </summary>
        Task<FirmwareSource> UpsertAsync(FirmwareSource soucerecord);

        /// <summary>
        /// Deletes an order.
        /// </summary>
        Task<FirmwareSource> DeleteAsync(long firmwaresourceID);
    }
}
