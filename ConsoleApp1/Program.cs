﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp1
{
    using System;
    using System.IO;
    using System.Diagnostics;
    using System.Reflection;
    using System.Windows.Forms;

    
    class Program
    {
        [STAThread]
        static int Main(string[] args)
        {
            int iReturn = 9;
            try
            {
                if (args.Length != 0)
                {
                    string executable = args[2];
                    string delay = args[3];
                    int iDelay = 30000;

                    if(String.IsNullOrEmpty(delay) != true)
                        iDelay = Convert.ToInt32(delay);

                    /*uncomment the below three lines if the exe file is in the Assets  
                     folder of the project and not installed with the system*/
                    string path=Assembly.GetExecutingAssembly().CodeBase;
                    string directory=Path.GetDirectoryName(path);
                    //Process.Start(directory+"\\"+executable);
                    //Process.Start(executable);
                    //Process p = Process.Start("C:\\SeggerFiles\\" + executable);
                    
                    Process p = new Process();
                    //JRR11022018 executable = "jflasher_Start_Executable.exe";
                    //JRR11022018 p.StartInfo.FileName = ("C:\\SeggerFiles\\" + executable);
                    p.StartInfo.FileName = (executable);
                    //p.EnableRaisingEvents = true;
                    //p.Exited += new EventHandler(myApplication_HasExited);

                    if(p.Start() != true)
                    {
                        iReturn = 8;
                        Clipboard.SetText(iReturn.ToString() + " - Console Return Code Failed");
                    }
                    else
                    {
                        if (p.WaitForExit(iDelay) != true)
                        {
                            iReturn = 7;
                            Clipboard.SetText(iReturn.ToString() + " - Console Return Code Failed");
                        }
                        else
                        {
                            iReturn = p.ExitCode;
                            Clipboard.SetText(iReturn.ToString() + " - Console Return Code Success");
                        }
                    }

                    return iReturn;
                }
                else
                {
                    Clipboard.SetText(iReturn.ToString() + " - Console Return Code Failed");
                    return iReturn;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            Clipboard.SetText(iReturn.ToString() + " - Console Return Code Failed");
            return iReturn;
        }
    }
}
