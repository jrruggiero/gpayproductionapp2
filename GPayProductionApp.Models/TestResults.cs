﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Data.Core;

namespace GPayProductionApp.Models
{
    public class TestResults : DbObject
    {
        public string Operator { get; set; }

        public string SalesOrder { get; set; }

        public string KitNumber { get; set; }

        public string PartNumber { get; set; }

        public string Revision { get; set; }

        public string FWVersion { get; set; }

        public string SerialNumberMACAddress { get; set; }

        public DateTime DTFixtureTest { get; set; }

        public DateTime DTWiredTest { get; set; }

        public string DeviceTypeUsed { get; set; }

        public string TestStationID { get; set; }

        public string Results { get; set; }

        public override string ToString()
        {
            return $"{SalesOrder} {PartNumber}-{Revision} {FWVersion} Results: {Results}";
        }
    }
}
