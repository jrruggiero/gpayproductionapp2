﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Data.Core;

namespace GPayProductionApp.Models
{
    public class FirmwareSource : DbObject
    {
        [Display(Header = "Softare Path:", PlaceholderText = "Enter SW Path")]
        public string Filename { get; set; }

        public string Description { get; set; }

        [Display(Header = "PN:", PlaceholderText = "Enter Part Number")]
        public string PartNumber { get; set; }

        [Display(Header = "PN Rev:", PlaceholderText = "Enter Part Number Rev.")]
        public string Revision { get; set; }

        public string FWVersion { get; set; }

        public string ValidDeviceTypes { get; set; }

        public string QRCodePrefixInfo { get; set; }

        public DateTime DTAdded { get; set; }

        public int Active { get; set; }

        public override string ToString()
        {
            return $"{PartNumber}-{Revision}: {Description} - {Filename}";
        }
    }
}
