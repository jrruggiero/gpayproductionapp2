﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Data.Core;

namespace GPayProductionApp.Models
{
    public class ProgrammerResults : DbObject
    {
        [Display(Header = "User:", PlaceholderText = "Enter User")]
        public string Operator { get; set; }

        [Display(Header = "Sales Order:", PlaceholderText = "Enter Sales Order")]
        public string SalesOrder { get; set; }

        [Display(Header = "Part #:", PlaceholderText = "Enter Part Number")]
        public string PartNumber { get; set; }

        [Display(Header = "Part # Revision:", PlaceholderText = "Enter Part # Revision")]
        public string Revision { get; set; }

        [Display(Header = "Date Programmed:", PlaceholderText = "Enter Programmed Date")]
        public DateTime DateProgrammed { get; set; }

        [Display(Header = "Device Type:", PlaceholderText = "Enter Device")]
        public string DeviceTypeUsed { get; set; }

        [Display(Header = "Software Version:", PlaceholderText = "Enter SW Version")]
        public string FWVersion { get; set; }

        [Display(Header = "Results:", PlaceholderText = "Enter Results")]
        public string Results { get; set; }

        public override string ToString()
        {
            return $"{SalesOrder} {PartNumber}-{Revision} {FWVersion} Prog. Results: {Results}";
        }
    }
}
