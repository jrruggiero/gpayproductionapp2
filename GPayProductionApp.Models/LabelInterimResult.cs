﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Data.Core;

namespace GPayProductionApp.Models
{
    public class LabelInterimResult : DbObject
    {
        [Display(Header = "Kit #:", PlaceholderText = "Enter Kit #")]
        public string KitNumber { get; set; }

        [Display(Header = "Part #:", PlaceholderText = "Enter Part #")]
        public string PartNumber { get; set; }

        [Display(Header = "Part # Revision:", PlaceholderText = "Enter Part # Revision")]
        public string PartNumberRevision { get; set; }

        [Display(Header = "DateCode:", PlaceholderText = "Enter Datecode")]
        public string DateCode { get; set; }

        [Display(Header = "MAC Address:", PlaceholderText = "Enter MAC Address")]
        public string SerialNumberMACAddress { get; set; }

        [Display(Header = "QR Code Prefix", PlaceholderText = "Enter QR Code Prefix")]
        public string QRCodePrefixInfo { get; set; }

        public override string ToString()
        {
            return $"PartNo:{PartNumber}, Rev:{PartNumberRevision}, Date:{DateCode}, MAC:{SerialNumberMACAddress}";
        }
    }
}
