﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Data.Core;

namespace GPayProductionApp.Models
{
    public class FinalResults : DbObject
    {
        [Display(Header = "User:", PlaceholderText = "Enter User")]
        public string Operator { get; set; }

        [Display(Header = "Sales Order:", PlaceholderText = "Enter Sales Order")]
        public string SalesOrder { get; set; }

        [Display(Header = "Firmware Version:", PlaceholderText = "Enter Firmware Version")]
        public string FWVersion { get; set; }

        [Display(Header = "Kit #:", PlaceholderText = "Enter Kit Number")]
        public string KitNumber { get; set; }

        [Display(Header = "Part #:", PlaceholderText = "Enter Part Number")]
        public string PartNumber { get; set; }

        [Display(Header = "Part # Revision:", PlaceholderText = "Enter Part # Revision")]
        public string Revision { get; set; }

        [Display(Header = "DateCode:", PlaceholderText = "Enter DateCode")]
        public string DateCode { get; set; }

        [Display(Header = "MAC Address:", PlaceholderText = "Enter MAC Address")]
        public string SerialNumberMACAddress { get; set; }

        [Display(Header = "QR Code Content:", PlaceholderText = "Enter QR Code Content")]
        public string QRCodeContent { get; set; }

        [Display(Header = "Date Labeled:", PlaceholderText = "Enter Labeled Date")]
        public DateTime DTLabeled { get; set; }

        [Display(Header = "Label Station ID:", PlaceholderText = "Enter Label Station ID")]
        public string LabelStationID { get; set; }

        public override string ToString()
        {
            return $"SalesOrder:{SalesOrder} PartNo:{PartNumber}, Rev:{Revision}, Date:{DateCode}, MAC:{SerialNumberMACAddress}";
        }
    }
}
