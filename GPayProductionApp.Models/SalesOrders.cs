﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Data.Core;

namespace GPayProductionApp.Models
{
    public class SalesOrders : DbObject
    {
        [Display(Header = "Sales Order:", PlaceholderText = "Enter Sales Order #")]
        public string SalesOrder { get; set; }

        [Display(Header = "Customer:", PlaceholderText = "Enter Customer")]
        public string Customer { get; set; }

        [Display(Header = "Description:", PlaceholderText = "Enter Description")]
        public string Description { get; set; }

        [Display(Header = "Kit #:", PlaceholderText = "Enter Kit #")]
        public string KitNumber { get; set; }

        [Display(Header = "Part #:", PlaceholderText = "Enter Part #")]
        public string PartNumber { get; set; }

        [Display(Header = "Part # Revision:", PlaceholderText = "Enter Part # Rev.")]
        public string Revision { get; set; }

        [Display(Header = "Firmware Version:", PlaceholderText = "Enter Firmware Version")]
        public string FWVersion { get; set; }

        [Display(Header = "Quantity Desired:", PlaceholderText = "Enter Quantity Needed")]
        public int NumNeeded { get; set; }

        [Display(Header = "Quantity Done:", PlaceholderText = "Enter Quantity Done")]
        public int NumDone { get; set; }

        [Display(Header = "Date Desired:", PlaceholderText = "Enter Date Desired")]
        public DateTime DTNeeded { get; set; }

        [Display(Header = "Date Completed:", PlaceholderText = "Enter Date Completed")]
        public DateTime DTDone { get; set; }

        [Display(Header = "Order Status:", PlaceholderText = "Enter Order Status")]
        public int OrderDone { get; set; }

        public override string ToString()
        {
            return $"{SalesOrder} {Customer} {PartNumber}-{Revision} Quantity:{NumNeeded}";
        }
    }
}
