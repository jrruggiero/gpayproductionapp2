﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;

//using SDKTemplate;
//using System;

using System.Threading.Tasks;
using System.IO;
using System.Text;

using Windows.Networking;

using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.System;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.ApplicationModel;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage.AccessCache;
using Windows.UI.Xaml.Media;
using System.Collections.Generic;


using System.Collections.ObjectModel;

using GPayProductionApp.Models;
using GPayProductionApp2.Services;



namespace GPayProductionApp2.Views
{
    public sealed partial class ProgramDevicePage : Page, INotifyPropertyChanged
    {
        // A pointer back to the main page is required to display status messages.
        private MainPage rootPage;

        public ProgramDevicePage()
        {
            InitializeComponent();

            if(MainPage.fwSource == null)
            {
                RunEraseProgram.IsEnabled = false;
                RunEraseProgramStart.IsEnabled = false;
            }
            else
            {
                RunEraseProgram.IsEnabled = false;
                RunEraseProgramStart.IsEnabled = true;
            }

            Done.IsEnabled = false;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            rootPage = MainPage.Current;

            // Attach handlers for suspension to stop the watcher when the App is suspended.
            App.Current.Suspending += App_Suspending;
            App.Current.Resuming += App_Resuming;

            rootPage.NotifyUser("Press Run to start watcher.", MainPage.NotifyType.StatusMessage);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            // Remove local suspension handlers from the App since this page is no longer active.
            App.Current.Suspending -= App_Suspending;
            App.Current.Resuming -= App_Resuming;

            rootPage.NotifyUser("Navigating away. Watcher stopped.", MainPage.NotifyType.StatusMessage);

            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {
            rootPage.NotifyUser("App suspending. Watcher stopped.", MainPage.NotifyType.StatusMessage);
        }

        private void App_Resuming(object sender, object e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        public enum NotifyType
        {
            StatusMessage,
            ErrorMessage
        };

        public void NotifyUser(string strMessage, NotifyType type)
        {
            // If called from the UI thread, then update immediately.
            // Otherwise, schedule a task on the UI thread to perform the update.
            if (Dispatcher.HasThreadAccess)
            {
                UpdateStatus(strMessage, type);
            }
            else
            {
                var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UpdateStatus(strMessage, type));
            }
        }

        private void UpdateStatus(string strMessage, NotifyType type)
        {
            switch (type)
            {
                case NotifyType.StatusMessage:
                    //StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Green);
                    break;
                case NotifyType.ErrorMessage:
                    //StatusBorder.Background = new SolidColorBrush(Windows.UI.Colors.Red);
                    break;
            }

           // StatusBlock.Text = strMessage;

            // Raise an event if necessary to enable a screen reader to announce the status update.
//            var peer = FrameworkElementAutomationPeer.FromElement(StatusBlock);
//            if (peer != null)
//            {
//                peer.RaiseAutomationEvent(AutomationEvents.LiveRegionChanged);
//            }
        }

        private void NotifyUserFromAsyncThread(string strMessage, NotifyType type)
        {
            //var ignore = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => rootPage.NotifyUser(strMessage, type));
            NotifyUser(strMessage, type);
        }

        private void Command_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private async void RunEraseProgram_Click(object sender, RoutedEventArgs e)
        {
            StorageFile resultFile = null;
            string sResult = "";

            if (rootPage.storagefileSegger == null
                || MainPage.oSalesOrder == null
                || MainPage.fwSource == null)
                return;

            RunEraseProgram.IsEnabled = false;

            resultFile = (StorageFile) await rootPage.storagefileSegger.TryGetItemAsync("SeggerLastResult.txt");

            if (resultFile != null)
                await resultFile.DeleteAsync();

            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("JFlasherEraseProgramGroup");

            try
            {
                do
                {
                    await Task.Delay(1000);

                    resultFile = (StorageFile)await rootPage.storagefileSegger.TryGetItemAsync("SeggerLastResult.txt");

                    if (resultFile != null)
                        sResult = await Windows.Storage.FileIO.ReadTextAsync(resultFile);
                }
                while (String.Compare(sResult, "", true) == 0);

                var iReturnCode = decimal.Parse(sResult[0].ToString());

                if (iReturnCode == 0)
                    Done.IsEnabled = true;

                Command.Text = "ResultCode=" + sResult;
            }
            catch (Exception ex)
            {
                Command.Text = ex.ToString();
            }

            WriteProgrammingResults(Command.Text.ToString());

            RunEraseProgram.IsEnabled = true;
        }

        private async void RunEraseProgramStart_Click(object sender, RoutedEventArgs e)
        {
            StorageFile resultFile = null;
            string sResult = "";

            if (rootPage.storagefileSegger == null
                || MainPage.oSalesOrder == null
                || MainPage.fwSource == null)
                return;

            RunEraseProgramStart.IsEnabled = false;

            resultFile = (StorageFile) await rootPage.storagefileSegger.TryGetItemAsync("SeggerLastResult.txt");

            if (resultFile != null)
                await resultFile.DeleteAsync();

            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("JFlasherEraseProgramStartGroup");

            try
            {
                do
                {
                    await Task.Delay(1000);

                    resultFile = (StorageFile)await rootPage.storagefileSegger.TryGetItemAsync("SeggerLastResult.txt");

                    if (resultFile != null)
                        sResult = await Windows.Storage.FileIO.ReadTextAsync(resultFile);
                }
                while (String.Compare(sResult, "", true) == 0);

                var iReturnCode = decimal.Parse(sResult[0].ToString());

                if (iReturnCode == 0)
                    Done.IsEnabled = true;

                Command.Text = "ResultCode=" + sResult;
            }
            catch (Exception ex)
            {
                Command.Text = ex.ToString();
            }

            RunEraseProgramStart.IsEnabled = true;
        }

        private void Done_Click(object sender, RoutedEventArgs e)
        {
            Done.IsEnabled = false;
            WriteProgrammingResults(Command.Text.ToString());
        }

        private async void WriteProgrammingResults(string sProgrammerResults)
        {
            ProgrammerResults itemProgrammed = new ProgrammerResults();

            DateTime dt = DateTime.Now;

            itemProgrammed.FWVersion = MainPage.oSalesOrder.FWVersion;
            itemProgrammed.PartNumber = MainPage.szPartNumber;
            itemProgrammed.Revision = MainPage.szPartNumberRev;
            itemProgrammed.FWVersion = MainPage.fwSource.FWVersion;
            itemProgrammed.Results = sProgrammerResults;
            itemProgrammed.SalesOrder = MainPage.oSalesOrder.SalesOrder.ToString();
            itemProgrammed.DateProgrammed = dt;
            itemProgrammed.DeviceTypeUsed = MainPage.fwSource.ValidDeviceTypes;
            itemProgrammed.Operator = MainPage.szOperator;

            await SampleDataService.UpdateProgrammerResults(itemProgrammed);
        }
    }
}
