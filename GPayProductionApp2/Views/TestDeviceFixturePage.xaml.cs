﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Data;
using System.Diagnostics;

//using SDKTemplate;
//using System;

using System.Threading.Tasks;
using System.IO;
using System.Text;

using Windows.Networking;

using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.System;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.ApplicationModel;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage.AccessCache;
using Windows.UI.Xaml.Media;
using System.Collections.Generic;


using GPayProductionApp2.Helpers;
using GPayProductionApp2.Services;
using GPayProductionApp.Models;


namespace GPayProductionApp2.Views
{
    public sealed partial class TestDeviceFixturePage : Page, INotifyPropertyChanged
    {
        //private ElementTheme _elementTheme = ThemeSelectorService.Theme;
        // Used for XAML static binding.
        private bool _TestPassed;
        private bool _TestFailed;
        private MainPage rootPage;

        public bool TestPassed
        {
            get { return _TestPassed; }

            set { Set(ref _TestPassed, value); }
        }

        public bool TestFailed
        {
            get { return _TestFailed; }

            set { Set(ref _TestFailed, value); }
        }

        public TestDeviceFixturePage()
        {
            InitializeComponent();

            /*
            if (MainPage.fwSource == null)
                RunStart.IsEnabled = false;
            else
                RunStart.IsEnabled = true;
            */

            Yes.IsEnabled = false;
            No.IsEnabled = false;
            TestFailed = false;
            TestPassed = false;
            Done.IsEnabled = false;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            rootPage = MainPage.Current;

            // Attach handlers for suspension to stop the watcher when the App is suspended.
            App.Current.Suspending += App_Suspending;
            App.Current.Resuming += App_Resuming;

            rootPage.NotifyUser("Press Run to start watcher.", MainPage.NotifyType.StatusMessage);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            // Remove local suspension handlers from the App since this page is no longer active.
            App.Current.Suspending -= App_Suspending;
            App.Current.Resuming -= App_Resuming;

            rootPage.NotifyUser("Navigating away. Watcher stopped.", MainPage.NotifyType.StatusMessage);

            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {
            rootPage.NotifyUser("App suspending. Watcher stopped.", MainPage.NotifyType.StatusMessage);
        }

        private void App_Resuming(object sender, object e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void TestResultChanged_Checked(object sender, RoutedEventArgs e)
        {
            var param = (sender as RadioButton)?.CommandParameter;

            Done.IsEnabled = true;
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        public enum NotifyType
        {
            StatusMessage,
            ErrorMessage
        };

        public void NotifyUser(string strMessage, NotifyType type)
        {
            // If called from the UI thread, then update immediately.
            // Otherwise, schedule a task on the UI thread to perform the update.
            if (Dispatcher.HasThreadAccess)
            {
                UpdateStatus(strMessage, type);
            }
            else
            {
                var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UpdateStatus(strMessage, type));
            }
        }

        private void UpdateStatus(string strMessage, NotifyType type)
        {

        }

        private void NotifyUserFromAsyncThread(string strMessage, NotifyType type)
        {
            //var ignore = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => rootPage.NotifyUser(strMessage, type));
            NotifyUser(strMessage, type);
        }

        private async void RunStart_Click(object sender, RoutedEventArgs e)
        {
            StorageFile resultFile = null;
            string sResult = "";

            /*
            if (rootPage.storagefileSegger == null)
            {
                Command.Text = "Segger Directory Not configured";
                TestPassed = false;
                TestFailed = true;
                return;
            }
            */

            if (rootPage.storagefileSegger == null)
                return;

            RunStart.IsEnabled = false;

            resultFile = (StorageFile) await rootPage.storagefileSegger.TryGetItemAsync("SeggerLastResult.txt");

            if(resultFile != null)
                await resultFile.DeleteAsync();

            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("JFlasherStartGroup");

            try
            {
                do
                {
                    await Task.Delay(1000);

                    resultFile = (StorageFile) await rootPage.storagefileSegger.TryGetItemAsync("SeggerLastResult.txt");

                    if (resultFile != null)
                        sResult = await Windows.Storage.FileIO.ReadTextAsync(resultFile);
                }
                while (String.Compare(sResult, "", true) == 0);

                //var iReturnCode = decimal.Parse(sResult[0].ToString());

                Command.Text = "ResultCode=" + sResult;

                //Only if the test passed should we enable these
                Yes.IsEnabled = true;
                No.IsEnabled = true;
            }
            catch(Exception ex)
            {
                Command.Text = ex.ToString();
            }

            RunStart.IsEnabled = true;
        }

        private async void Done_Click(object sender, RoutedEventArgs e)
        {
            //Record test results here, then reset buttons
            TestResults tstResult = new TestResults();

            tstResult.Operator = MainPage.szOperator;
            tstResult.SalesOrder = MainPage.oSalesOrder.SalesOrder.ToString();
            tstResult.KitNumber = MainPage.szKitNumber;     //JRR01072019
            tstResult.PartNumber = MainPage.szPartNumber;
            tstResult.Revision = MainPage.szPartNumberRev;
            tstResult.DeviceTypeUsed = MainPage.fwSource.ValidDeviceTypes;
            tstResult.DTFixtureTest = DateTime.Now;
            tstResult.FWVersion = MainPage.fwSource.FWVersion;
            tstResult.SerialNumberMACAddress = MainPage.szMACAddress;
            tstResult.TestStationID = "1";
            tstResult.Results = "FixureTest," + Command.Text + ",Passed=" + TestPassed.ToString();

            await SampleDataService.UpdateTestResults(tstResult);

            Command.Text = "";

            Yes.IsEnabled = false;
            No.IsEnabled = false;
            TestFailed = false;
            TestPassed = false;
            Done.IsEnabled = false;
        }

        private void Command_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
