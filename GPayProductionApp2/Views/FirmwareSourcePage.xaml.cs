﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using GPayProductionApp.Models;
using GPayProductionApp2.Services;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

using Windows.Storage;

using Microsoft.Toolkit.Uwp.UI.Controls;

using System.Collections.Generic;

namespace GPayProductionApp2.Views
{
    public sealed partial class FirmwareSourcePage : Page, INotifyPropertyChanged
    {
        private FirmwareSource _selected;
        List<StorageFile> hexfiles = new List<StorageFile>();
        private MainPage rootPage = null;

        public FirmwareSource Selected
        {
            get { return _selected; }
            set {  Set(ref _selected, value);
                if ((_selected != null) && (rootPage != null))
                {
                    MainPage.fwSource = _selected;


                }
            }
        }

        public ObservableCollection<FirmwareSource> FirmwareSource { get; private set; } = new ObservableCollection<FirmwareSource>();


        // TODO WTS: Change the grid as appropriate to your app, adjust the column definitions on TelerikDataGrid1Page.xaml.
        // For help see http://docs.telerik.com/windows-universal/controls/raddatagrid/gettingstarted
        // You may also want to extend the grid to work with the RadDataForm http://docs.telerik.com/windows-universal/controls/raddataform/dataform-gettingstarted
        public FirmwareSourcePage()
        {
            InitializeComponent();

            Loaded += FirmwareSourcePage_Loaded;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Initialize();
            rootPage = MainPage.Current;
        }

        protected override async void OnNavigatedFrom(NavigationEventArgs e)
        {

            if(_selected != null)
            {
                foreach (StorageFile file2 in hexfiles)
                {
                    if (String.Compare(file2.Name, _selected.Filename, true) == 0)
                    {
                        await file2.CopyAsync(rootPage.storagefileSegger,
                                              "CurrentFirmware.hex",
                                              NameCollisionOption.ReplaceExisting);
                    }
                }
            }
            else
            {
                if (rootPage.storagefileSegger != null)
                {
                    StorageFile file = (StorageFile) await rootPage.storagefileSegger.TryGetItemAsync("CurrentFirmware.hex");

                    if (file != null)
                        await file.DeleteAsync();
                }
            }


            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {

        }

        private void App_Resuming(object sender, object e)
        {

        }

        private async void FirmwareSourcePage_Loaded(object sender, RoutedEventArgs e)
        {
            //
            // See which firmwarre files are present in the firmware directory
            //
            if (rootPage.storagefileFirmware != null)
            {
                IReadOnlyList<StorageFile> files = await rootPage.storagefileFirmware.GetFilesAsync();

                if (files.Count > 0)
                {
                    // Application now has read/write access to the picked file(s)
                    foreach (StorageFile file2 in files)
                    {
                        if (String.Compare(file2.FileType, ".hex", true) == 0)
                            hexfiles.Add(file2);
                    }
                }
            }

            //
            // Clear the main firmware table and get the entries from the database
            //
            FirmwareSource.Clear();

            if (rootPage != null)
            {
                var data = await SampleDataService.GetAllFirmwareSource(MainPage.szPartNumber,
                                                                        MainPage.szPartNumberRev,
                                                                        1);

                foreach (var item in data)
                {
                    foreach (StorageFile file2 in hexfiles)
                    {
                        if (String.Compare(file2.Name, item.Filename, true) == 0)
                        {
                            FirmwareSource.Add(item);
                        }
                    }
                }
            }

            /*
            if (MasterDetailsViewControl.ViewState == MasterDetailsViewState.Both)
            {
                Selected = SampleItems.First();
            }
            */




        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
