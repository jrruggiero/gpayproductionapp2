﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;

//using SDKTemplate;
//using System;

using System.Threading.Tasks;
using System.IO;
using System.Text;

using Windows.Networking;

using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.System;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.ApplicationModel;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage.AccessCache;
using Windows.UI.Xaml.Media;
using System.Collections.Generic;


using GPayProductionApp2.Helpers;
using GPayProductionApp2.Services;
using GPayProductionApp.Models;

namespace GPayProductionApp2.Views
{
    public sealed partial class PrintLabelPage : Page, INotifyPropertyChanged
    {
        // A pointer back to the main page is required to display status messages.
        private MainPage rootPage;
        private bool _PrintLabelPassed = false;
        private bool _PrintLabelFailed = false;

        public bool PrintLabelPassed
        {
            get { return _PrintLabelPassed; }

            set { Set(ref _PrintLabelPassed, value); }
        }

        public bool PrintLabelFailed
        {
            get { return _PrintLabelFailed; }

            set { Set(ref _PrintLabelFailed, value); }
        }

        public PrintLabelPage()
        {
            InitializeComponent();

            Yes.IsEnabled = false;
            No.IsEnabled = false;
            PrintLabelFailed = false;
            PrintLabelPassed = false;
            Done.IsEnabled = false;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            rootPage = MainPage.Current;

            // Attach handlers for suspension to stop the watcher when the App is suspended.
            App.Current.Suspending += App_Suspending;
            App.Current.Resuming += App_Resuming;

            rootPage.NotifyUser("Press Run to start watcher.", MainPage.NotifyType.StatusMessage);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            // Remove local suspension handlers from the App since this page is no longer active.
            App.Current.Suspending -= App_Suspending;
            App.Current.Resuming -= App_Resuming;

            rootPage.NotifyUser("Navigating away. Watcher stopped.", MainPage.NotifyType.StatusMessage);

            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {
            rootPage.NotifyUser("App suspending. Watcher stopped.", MainPage.NotifyType.StatusMessage);
        }

        private void App_Resuming(object sender, object e)
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        public enum NotifyType
        {
            StatusMessage,
            ErrorMessage
        };

        public void NotifyUser(string strMessage, NotifyType type)
        {
            // If called from the UI thread, then update immediately.
            // Otherwise, schedule a task on the UI thread to perform the update.
            if (Dispatcher.HasThreadAccess)
            {
                UpdateStatus(strMessage, type);
            }
            else
            {
                var task = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UpdateStatus(strMessage, type));
            }
        }

        private void UpdateStatus(string strMessage, NotifyType type)
        {
        }

        private void NotifyUserFromAsyncThread(string strMessage, NotifyType type)
        {
            //var ignore = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => rootPage.NotifyUser(strMessage, type));
            NotifyUser(strMessage, type);
        }

        public static Task<bool> ExecuteBatchFile(string BatchFilePath, string BatchFileDirectory)
        {
            try
            {
                Task<bool> executeBatchFileTask = Task.Run<bool>(() =>
                {
                    bool hasProcessExited = false;
                    ProcessStartInfo startInfo = new ProcessStartInfo()
                    {
                        FileName = BatchFilePath,
                        CreateNoWindow = false,
                        UseShellExecute = true,
                        WindowStyle = ProcessWindowStyle.Normal,
                        WorkingDirectory = BatchFileDirectory
                    };

                    // Start the process with the info we specified.
                    // Call WaitForExit and then the using-statement will close.
                    using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startInfo))
                    {
                        while (!exeProcess.HasExited)
                        {
                            //Do nothing  
                        }
                        hasProcessExited = true;
                    }

                    return hasProcessExited;
                });
                return executeBatchFileTask;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static async Task LaunchExeAsync(string exeId, bool prompt = true)
        {
            //var openPicker = new FileOpenPicker();
            //openPicker.ViewMode = PickerViewMode.Thumbnail;
            //openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary; // PickerLocationId.DocumentsLibrary;
            //openPicker.FileTypeFilter.Add(".bat");
            //openPicker.FileTypeFilter.Add(".png");
            //openPicker.FileTypeFilter.Add(".jpg");
            //openPicker.FileTypeFilter.Add(".jpeg");

            //StorageFile file = await openPicker.PickSingleFileAsync();


            //exeId = "." + exeId;
            StorageFolder storageFolder = KnownFolders.PicturesLibrary;
            //StorageFolder storageFolder = KnownFolders.DocumentsLibrary;
            //StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile file = null;

            try 
            {
                file = await storageFolder.GetFileAsync(exeId);
                //file = await ApplicationData.Current.LocalFolder.GetFileAsync(exeId);
            }
            catch (FileNotFoundException)
            {

            }
            
            LauncherOptions launchopt = new LauncherOptions();
            launchopt.DisplayApplicationPicker = true;
            launchopt.TreatAsUntrusted = true;


            //bool x = await Launcher.LaunchFileAsync(file, launchopt);
            bool x = await Launcher.LaunchFileAsync(file);
        }

        private void Command_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private static async Task<string> FetchClipboard()
        {
            DataPackageView dpv = Clipboard.GetContent();

            if(dpv.Contains(StandardDataFormats.Text))
            {
                string text = await dpv.GetTextAsync();
                return text;
            }
            return "";
        }

        private async void RunZebra_Click(object sender, RoutedEventArgs e)
        {
            StorageFile resultFilePrint = null;
            string sResultPrint = "";

            if (rootPage.storagefileZebra == null)
                return;

            LabelInterimResult itemRead = await SampleDataService.GetLabelInterimResult(1);

            if (itemRead == null)
                itemRead = new LabelInterimResult();

            RunZebra.IsEnabled = false;

            await PrintDirectOperation(itemRead);

            /*
            resultFile = (StorageFile) await rootPage.storagefileZebra.TryGetItemAsync("ZebraLastResult.txt");

            if (resultFile != null)
                await resultFile.DeleteAsync();

            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("ZebraStartupGroup");

            try
            {
                do
                {
                    await Task.Delay(1000);

                    resultFile = (StorageFile)await rootPage.storagefileZebra.TryGetItemAsync("ZebraLastResult.txt");

                    if (resultFile != null)
                        sResult = await Windows.Storage.FileIO.ReadTextAsync(resultFile);
                }
                while (String.Compare(sResult, "", true) == 0);

                var iReturnCode = decimal.Parse(sResult[0].ToString());

                Command.Text = "ResultCode = " + sResult;

                if(iReturnCode == 0)
                {
                    Yes.IsEnabled = true;
                    No.IsEnabled = true;
                    Done.IsEnabled = true;
                }
            }
            catch(Exception ex)
            {
                Command.Text = ex.ToString();
            }
            */

            resultFilePrint = (StorageFile)await rootPage.storagefileZebra.TryGetItemAsync("PrintDirectLastResult.txt");

            if (resultFilePrint != null)
                await resultFilePrint.DeleteAsync();

            await FullTrustProcessLauncher.LaunchFullTrustProcessForCurrentAppAsync("PrintDirectStartupGroup");

            try
            {
                do
                {
                    await Task.Delay(1000);

                    resultFilePrint = (StorageFile)await rootPage.storagefileZebra.TryGetItemAsync("PrintDirectLastResult.txt");

                    if (resultFilePrint != null)
                        sResultPrint = await Windows.Storage.FileIO.ReadTextAsync(resultFilePrint);
                }
                while (String.Compare(sResultPrint, "", true) == 0);

                var iReturnCode = decimal.Parse(sResultPrint[0].ToString());

                Command.Text = "ResultCode = " + sResultPrint;

                if (iReturnCode == 0)
                {
                    Yes.IsEnabled = true;
                    No.IsEnabled = true;
                    Done.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                Command.Text = ex.ToString();
            }

            RunZebra.IsEnabled = true;
        }

        private async void Done_Click(object sender, RoutedEventArgs e)
        {
            if (PrintLabelPassed == true)
            {
                LabelInterimResult itemRead = await SampleDataService.GetLabelInterimResult(1);

                if (itemRead == null)
                    itemRead = new LabelInterimResult();

                //Record test results here, then reset buttons
                FinalResults finalResult = new FinalResults();

                finalResult.Operator = MainPage.szOperator;
                finalResult.SalesOrder = MainPage.oSalesOrder.SalesOrder;

                if (MainPage.fwSource != null)
                    finalResult.FWVersion = MainPage.fwSource.FWVersion;
                else
                    finalResult.FWVersion = "Unknown";

                finalResult.KitNumber = itemRead.KitNumber;     //JRR01072019
                finalResult.PartNumber = itemRead.PartNumber;
                finalResult.Revision = itemRead.PartNumberRevision;
                finalResult.DateCode = itemRead.DateCode;
                finalResult.SerialNumberMACAddress = itemRead.SerialNumberMACAddress;
                finalResult.QRCodeContent = itemRead.QRCodePrefixInfo
                                            //+ "GPAY?"
                                            + "PartNo=" + itemRead.PartNumber + "&"
                                            + "Revision=" + itemRead.PartNumberRevision + "&"
                                            + "DateCode=" + itemRead.DateCode + "&"
                                            + "MAC=" + itemRead.SerialNumberMACAddress;
                finalResult.DTLabeled = DateTime.Now;
                finalResult.LabelStationID = "1";

                try
                {
                    await SampleDataService.UpdateFinalResults(finalResult);
                }
                catch
                {

                }

                Command.Text = "Done";

                await SampleDataService.DeleteLabelInterimResult(1);
            }

            Yes.IsEnabled = false;
            No.IsEnabled = false;
            PrintLabelFailed = false;
            PrintLabelPassed = false;
            Done.IsEnabled = false;
        }

        private void Yes_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void No_Checked(object sender, RoutedEventArgs e)
        {

        }

        private async Task<bool> PrintDirectOperation(LabelInterimResult itemRed)
        {
            StorageFile resultFile = null;
            StorageFile resultFileSimple = null;
            string sResult = "";
            string sResultComplex = "";
            string sResultSimple = "";
            string sMACAddress = itemRed.SerialNumberMACAddress; // MainPage.szMACAddress; // "11:22:33:44:55:66";
            string sDateCode = itemRed.DateCode; // "21278";
            string sRev = itemRed.PartNumberRevision; // MainPage.szPartNumberRev; // "A";
            string sKitNo = itemRed.KitNumber; // MainPage.szKitNumber; // "5004-102-71-CL";
            string sPartNo = itemRed.PartNumber; // MainPage.szPartNumber; // "A006444-2";

            string sXZStart = "^XA\n^MMT\n^PW305\n^LL203\n^LS-20\n";
            string sGreenwaldText = "^FO35,6^GFA,213,464,16,:Z64:eJxjYBiswI7xAZDkfwDjy4H5ch9gfD4GEN/CAJVfIJAnw85U+HGeDR8fmM/4r1z23I+Hff/55MF8hm+PLY5LPO5x57OA8J8d3vBcJvkMO4Rf+OPB//3PZYpP2EPlCx4cMGCXMbvxHc5/YMEuY/LjOYRvAeHbfIDybQoe1M9/LiNT8J7PHuz+jx/s+Pv3SSRU/5eHO52GAABohEUL:8320\n";
            string sMultiPayGraphic = "^FO17,35^GFA,629,1020,20,:Z64:eJx9kr9rk0EYx7+X915yJPFyQwsdKmQUFzMIvpPcQcE1hYJLhUAXx4rF+UI2hfwNRZdSoXNRCAnWzcE/oMPFd3GqHVOF1PuVt8i9+Cx3fPnyeZ7newfcFdNI6l4qoWtSjRzWGKc1Wg3wYZY23iQy0R6ZtDH5XtN4WaTahghn67Rq9pLzCNmtbPt5J1x6v9+Dxb7jSfQJChqvqhku795O8TRcdfklsHfyS2wHTe7O/dkwH1Zr37L/I0JeHeEoXPeEDON9vrnCVdB4cezPfP/FBfkWtDxy26vbSRbHyuKczTdPaNPPR/ayuE97ejbh3kdux9XmHbrt1yRkFpVW4xMvbBxDEL22kecD2rc8CQK55v1a8p/WZ/RIm5Oedq3zrzMqLU9Zn7pUcEm3S9M1vMcOjjNdjkvdc7zXsgkqxLNDQihRnsn+6K62vvJk9HGChXbz0E3k6Igthfn9DhTx+Z2Bg/fZUI9PGcqwymP7Gpan4Hoqv3nj3PkeMDcfwyIkNLA+z1PYggoJXlc86yvPw2Pd8ShU+DDG+YrC8bheGJ/LzPOElHPkRPkI7Re2Pm6Gy5HOVgdV0nDZCbvCQOGfcsGxlkZSNJXAazRRo9X8+//UXy4Ocns=:6CDE\n";
            string sMACHdr = "^FT11,194^A0N,23,25^FH\\^CI28^FDMAC:^FS^CI27\n";
            string sDateHdr = "^FT11,100^A0N,14,15^FH\\^CI28^FDDate:^FS^CI27\n";
            string sRVHdr = "^FT11,141^A0N,14,15^FH\\^CI28^FDR/V:^FS^CI27\n";
            string sKNHdr = "^FT11,165^A0N,17,18^FH\\^CI28^FDK/N:^FS^CI27\n";
            string sPNHdr = "^FT11,120^A0N,14,15^FH\\^CI28^FDP/N:^FS^CI27\n";
            string sPN = "^FT46,165^A0N,17,18^FH\\^CI28^FD" + sKitNo + "^FS^CI27\n";
            string sKN = "^FT41,120^A0N,16,15^FH\\^CI28^FD" + sPartNo + "^FS^CI27\n";
            string sRV = "^FT40,141^A0N,16,18^FH\\^CI28^FD" + sRev + "^FS^CI27\n";
            string sDate = "^FT48,100^A0N,16,15^FH\\^CI28^FD" + sDateCode + "^FS^CI27\n";
            string sMAC = "^FT70,194^A0N,23,25^FH\\^CI28^FD" + sMACAddress + "^FS^CI27\n";
            string sQRCode1 = "^FT160,154^BQN,2,3\n";
            string sQRCode2 = "^FH\\^FDMA,"
                                + itemRed.QRCodePrefixInfo
                                //+ "GPAY?"
                                + "PartNo=" + itemRed.PartNumber
                                + "&Revision=" + itemRed.PartNumberRevision
                                + "&Date=" + itemRed.DateCode
                                + "&MAC=" + itemRed.SerialNumberMACAddress
                                + "^FS\n";
            string sPageCount = "^PQ2,0,1,Y";
            string sXZEnd = "";

            if (rootPage.storagefileZebra == null)
            {
                Command.Text = "Zebra Print Directory Not configured";
                return false;
            }

            resultFileSimple = (StorageFile)await rootPage.storagefileZebra.TryGetItemAsync("MultiPayLabel_Latest_Linear.prn");

            if (resultFileSimple != null)
                sResultSimple = await Windows.Storage.FileIO.ReadTextAsync(resultFileSimple);

            resultFile = (StorageFile)await rootPage.storagefileZebra.TryGetItemAsync("MultiPayLabel_Latest_MainQR.prn");

            if (resultFile != null)
                sResultComplex = await Windows.Storage.FileIO.ReadTextAsync(resultFile);

            try
            {
                //Parse MAC in simpler file and replace with new MAC
                {
                    var resultQuotedMAC = sResultSimple.Split('"').Where((s, i) => i % 2 == 1).ToList();

                    string sOldMACAddress = resultQuotedMAC[0];
                    string sResultSimple2;
                    string sDefaultMACAddress = "  :  :  :  :  :  ";


                    if (String.IsNullOrEmpty(sOldMACAddress))
                    {
                        if (String.IsNullOrEmpty(sMACAddress))
                        {
                            sResultSimple2 = sResultSimple.Replace("\"\"", "\"" + sDefaultMACAddress + "\"");
                        }
                        else
                            sResultSimple2 = sResultSimple.Replace("\"\"", "\"" + sMACAddress + "\"");

                        await Windows.Storage.FileIO.WriteTextAsync(resultFileSimple, sResultSimple2);
                    }
                    else 
                    {
                        if (sMACAddress == null)
                            sMACAddress = sDefaultMACAddress;

                        if (String.Compare(sOldMACAddress, sMACAddress) != 0)
                        {
                            sResultSimple2 = sResultSimple.Replace(sOldMACAddress, sMACAddress);

                            await Windows.Storage.FileIO.WriteTextAsync(resultFileSimple, sResultSimple2);
                        }
                    }
                }

                //Write out stuff to more complicated file
                {
                    sResult += sXZStart
                                + sGreenwaldText
                                + sMultiPayGraphic
                                + sMACHdr
                                + sDateHdr
                                + sRVHdr
                                + sKNHdr
                                + sPNHdr
                                + sPN
                                + sKN
                                + sRV
                                + sDate
                                + sMAC
                                + sQRCode1
                                + sQRCode2
                                + sPageCount
                                + sXZEnd;

                    string sTest = sResultComplex.Split(new string[] { "^XZ" }, StringSplitOptions.None)[1]
                                                 .Split(new string[] { "^XZ" }, StringSplitOptions.None)[0]
                                                 .Trim();

                    string sResultComplex2 = sResultComplex.Replace(sTest, sResult);

                    await Windows.Storage.FileIO.WriteTextAsync(resultFile, sResultComplex2);
                }
            }
            catch (Exception ex)
            {
                Command.Text = ex.ToString();
            }

            using (var filestream = await resultFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                filestream.Dispose();
            }

            using (var filestream = await resultFileSimple.OpenAsync(FileAccessMode.ReadWrite))
            {
                filestream.Dispose();
            }

            return true;
        }
    }
}
