﻿using System;
using System.Linq;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

using GPayProductionApp.Models;
using GPayProductionApp2.Services;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System.Threading.Tasks;

namespace GPayProductionApp2.Views
{
    public sealed partial class SalesOrdersPage : Page, INotifyPropertyChanged
    {
        private MainPage rootPage;
        private SalesOrders _selected;

        public SalesOrders Selected
        {
            get { return _selected; }

            set { Set(ref _selected, value);

                 if ((_selected != null) && (rootPage != null))
                 {
                     MainPage.oSalesOrder = _selected;
                     MainPage.szKitNumber = _selected.KitNumber;        //JRR01072019
                     MainPage.szPartNumber = _selected.PartNumber;
                     MainPage.szPartNumberRev = _selected.Revision;
                 }
            }
        }

        public ObservableCollection<SalesOrders> SalesOrders { get; private set; } = new ObservableCollection<SalesOrders>();

        // TODO WTS: Change the grid as appropriate to your app, adjust the column definitions on TelerikDataGridPage.xaml.
        // For help see http://docs.telerik.com/windows-universal/controls/raddatagrid/gettingstarted
        // You may also want to extend the grid to work with the RadDataForm http://docs.telerik.com/windows-universal/controls/raddataform/dataform-gettingstarted
        public SalesOrdersPage()
        {
            InitializeComponent();
            Loaded += SalesOrdersPage_Loaded;

            // create a DeleteCommand instance
            DeleteCommand = new DelegateCommand(ExecuteDeleteCommand);
            NewCommand = new DelegateCommand(ExecuteNewCommand);
            UpdateCommand = new DelegateCommand(ExecuteUpdateCommand);
            DataContext = this;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Initialize();
            rootPage = MainPage.Current;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {

        }

        private void App_Resuming(object sender, object e)
        {

        }

        private async void SalesOrdersPage_Loaded(object sender, RoutedEventArgs e)
        {
            await UpdateTelerikGridView();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        

        // This property will be bound to button's Command property for deleting item
        public ICommand DeleteCommand { set; get; }
        public ICommand NewCommand { set; get; }
        public ICommand UpdateCommand { set; get; }

        public async void ExecuteDeleteCommand(object param)
        {
            SalesOrders orderToDelete = (SalesOrders) param;

            if (orderToDelete != null)
            {
                var data = await SampleDataService.DeleteSalesOrder(orderToDelete);
            }

            await UpdateTelerikGridView();
        }
        public void ExecuteNewCommand(object param)
        {
            SalesOrders orderNew = new SalesOrders();

            Selected = orderNew;
        }
        public async void ExecuteUpdateCommand(object param)
        {
            SalesOrders orderUpdate = (SalesOrders)param;
            
            if(orderUpdate != null)
            {
                try
                {
                    var data = await SampleDataService.UpdateSalesOrder(orderUpdate);
                }
                catch(Exception ex)
                {
                    ContentDialog duplicateSalesOrder = new ContentDialog();
                    ContentDialogResult noUserResult;
                    duplicateSalesOrder.Title = "Duplicate Sales Order";
                    duplicateSalesOrder.Content = "Sales Order must be unique!";
                    duplicateSalesOrder.CloseButtonText = "OK";

                    noUserResult = await duplicateSalesOrder.ShowAsync();
                }
            }

            await UpdateTelerikGridView();
        }

        private async Task UpdateTelerikGridView()
        {
            SalesOrders.Clear();

            var data = await SampleDataService.GetAllSalesOrders();

            foreach (var item in data)
            {
                SalesOrders.Add(item);
            }

            //if (MasterDetailsViewControl.ViewState == MasterDetailsViewState.Both)
            /*
            if (SalesOrders.Count() > 0)
            {
                Selected = SalesOrders.First();
            }
            */
        }
    }
}
