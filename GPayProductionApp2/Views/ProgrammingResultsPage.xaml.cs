﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using GPayProductionApp.Models;
using GPayProductionApp2.Services;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Threading.Tasks;

using Telerik.Data.Core;
using System.Windows.Input;

namespace GPayProductionApp2.Views
{
    public sealed partial class ProgrammingResultsPage : Page, INotifyPropertyChanged
    {
        private ProgrammerResults _selected;

        public ObservableCollection<ProgrammerResults> ProgrammerResults { get; private set; } = new ObservableCollection<ProgrammerResults>();

        public ProgrammerResults Selected
        {
            get { return _selected; }
            set { Set(ref _selected, value); }
        }

        // TODO WTS: Change the grid as appropriate to your app, adjust the column definitions on TelerikDataGrid3Page.xaml.
        // For help see http://docs.telerik.com/windows-universal/controls/raddatagrid/gettingstarted
        // You may also want to extend the grid to work with the RadDataForm http://docs.telerik.com/windows-universal/controls/raddataform/dataform-gettingstarted
        public ProgrammingResultsPage()
        {
            InitializeComponent();

            //dataForm.Item = _contact;
            Loaded += ProgrammerResultsPage_Loaded;


            // create a DeleteCommand instance
            DeleteCommand = new DelegateCommand(ExecuteDeleteCommand);
            NewCommand = new DelegateCommand(ExecuteNewCommand);
            DataContext = this;
        }

        // This property will be bound to button's Command property for deleting item
        public ICommand DeleteCommand { set; get; }
        public ICommand NewCommand { set; get; }


        private void ExecuteDeleteCommand(object param)
        {

            //int id = (Int32)param;
            /*
            Customer cus = GetCustomerById(id);

            if (cus != null)
            {
                Customers.Remove(cus);
            }*/

        }

        void ExecuteNewCommand(object param)
        {

            //int id = (Int32)param;

            /*
            Customer cus = GetCustomerById(id);

            if (cus != null)
            {
                Customers.Remove(cus);
            }*/

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Initialize();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {

        }

        private void App_Resuming(object sender, object e)
        {

        }

        private async void ProgrammerResultsPage_Loaded(object sender, RoutedEventArgs e)
        {
            await UpdateProgrammerResultsView();
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private async Task UpdateProgrammerResultsView()
        {
            ProgrammerResults datum = null;

            ProgrammerResults.Clear();

            var data = await SampleDataService.GetProgrammerResults();

            foreach (var item in data)
            {
                if (datum == null)
                    datum = item;

                ProgrammerResults.Add(item);
            }

            //if (MasterDetailsViewControl.ViewState == MasterDetailsViewState.Both)
            //{
            Selected = datum;
            //}

        }
    }
}
