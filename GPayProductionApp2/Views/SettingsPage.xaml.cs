﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

using GPayProductionApp2.Helpers;
using GPayProductionApp2.Services;

using Windows.ApplicationModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

using Windows.Storage;
using Windows.Storage.Pickers;

using System.Collections.Generic;

namespace GPayProductionApp2.Views
{
    // TODO WTS: Add other settings as necessary. For help see https://github.com/Microsoft/WindowsTemplateStudio/blob/master/docs/pages/settings-codebehind.md
    // TODO WTS: Change the URL for your privacy policy in the Resource File, currently set to https://YourPrivacyUrlGoesHere
    public sealed partial class SettingsPage : Page, INotifyPropertyChanged
    {
        private ElementTheme _elementTheme = ThemeSelectorService.Theme;
        private MainPage rootPage;

        public ElementTheme ElementTheme
        {
            get { return _elementTheme; }

            set { Set(ref _elementTheme, value); }
        }

        private string _versionDescription;

        public string VersionDescription
        {
            get { return _versionDescription; }

            set { Set(ref _versionDescription, value); }
        }

        public SettingsPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Initialize();
            rootPage = MainPage.Current;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            int iValue;

            if (int.TryParse(RSSILevelInValue.Text, out iValue))
            {
                if (iValue > 0 || iValue < -100)
                    MainPage.sInRangeThresholdInDBm = -40;
                else
                    MainPage.sInRangeThresholdInDBm = (short)iValue;
            }
            else
            {
                MainPage.sInRangeThresholdInDBm = -40;
            }

            ApplicationData.Current.LocalSettings.Values.Remove("RSSILevelInValue");
            ApplicationData.Current.LocalSettings.Values.Add("RSSILevelInValue", MainPage.sInRangeThresholdInDBm);


            if (int.TryParse(RSSILevelOutValue.Text, out iValue))
            {
                if (iValue > 0 || iValue < -100)
                    MainPage.sOutOfRangeThresholdInDBm = -50;
                else
                    MainPage.sOutOfRangeThresholdInDBm = (short)iValue;
            }
            else
            {
                MainPage.sOutOfRangeThresholdInDBm = -50;
            }

            ApplicationData.Current.LocalSettings.Values.Remove("RSSILevelOutValue");
            ApplicationData.Current.LocalSettings.Values.Add("RSSILevelOutValue", MainPage.sOutOfRangeThresholdInDBm);


            if (int.TryParse(RSSITimeoutValue.Text, out iValue))
            {
                if (iValue <= 0 || iValue > 60000)
                    MainPage.msOutOfRangeTimeout = 2000;
                else
                    MainPage.msOutOfRangeTimeout = (short)iValue;
            }
            else
            {
                MainPage.msOutOfRangeTimeout = 2000;
            }

            ApplicationData.Current.LocalSettings.Values.Remove("RSSITimeoutValue");
            ApplicationData.Current.LocalSettings.Values.Add("RSSITimeoutValue", MainPage.msOutOfRangeTimeout);


            base.OnNavigatedFrom(e);
        }

        private void App_Suspending(object sender, Windows.ApplicationModel.SuspendingEventArgs e)
        {

        }

        private void App_Resuming(object sender, object e)
        {

        }

        private void Initialize()
        {
            VersionDescription = GetVersionDescription();

            RSSILevelInValue.Text = MainPage.sInRangeThresholdInDBm.ToString();
            RSSILevelOutValue.Text = MainPage.sOutOfRangeThresholdInDBm.ToString();
            RSSITimeoutValue.Text = MainPage.msOutOfRangeTimeout.ToString();

            Loaded += SettingsPage_Loaded;
        }

        private async void SettingsPage_Loaded(object sender, RoutedEventArgs e)
        {
            var fal = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList;

            //string mrutoken = "{2D402A29-9741-42E4-961B-C868032217BA}";

            try
            {
                rootPage.storagefileSegger = await fal.GetFolderAsync(MainPage.sSeggerDirToken);
                SeggerDirectoryPath.Text = rootPage.storagefileSegger.Path.ToString();
            }
            catch
            {
                SeggerDirectoryPath.Text = "";
            }

            try
            {
                rootPage.storagefileFirmware = await fal.GetFolderAsync(MainPage.sFirmwareDirToken);
                FirmwareDirectoryPath.Text = rootPage.storagefileFirmware.Path.ToString();
            }
            catch
            {
                FirmwareDirectoryPath.Text = "";
            }

            try
            {
                rootPage.storagefileZebra = await fal.GetFolderAsync(MainPage.sZebraDirToken);
                ZebraDirectoryPath.Text = rootPage.storagefileZebra.Path.ToString();
            }
            catch
            {
                ZebraDirectoryPath.Text = "";
            }

            try
            {
                DatabaseDirectoryPath.Text = ApplicationData.Current.GetPublisherCacheFolder("GPayProductionDatabase").Path.ToString();
            }
            catch
            {
                DatabaseDirectoryPath.Text = "";
            }

        }

        private string GetVersionDescription()
        {
            var appName = "AppDisplayName".GetLocalized();
            var package = Package.Current;
            var packageId = package.Id;
            var version = packageId.Version;

            return $"{appName} - {version.Major}.{version.Minor}.{version.Build}.{version.Revision}";
        }

        private async void ThemeChanged_CheckedAsync(object sender, RoutedEventArgs e)
        {
            var param = (sender as RadioButton)?.CommandParameter;

            if (param != null)
            {
                await ThemeSelectorService.SetThemeAsync((ElementTheme)param);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void Set<T>(ref T storage, T value, [CallerMemberName]string propertyName = null)
        {
            if (Equals(storage, value))
            {
                return;
            }

            storage = value;
            OnPropertyChanged(propertyName);
        }

        private void OnPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private async void SeggerDirectory_Click(object sender, RoutedEventArgs e)
        {
            FolderPicker openPicker = new FolderPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".hex");

            StorageFolder sf = await openPicker.PickSingleFolderAsync();

            if (sf != null)
            {
                IReadOnlyList<StorageFile> files = await sf.GetFilesAsync();
                List<StorageFile> hexfiles = new List<StorageFile>();
                if (files.Count > 0)
                {
                    // Application now has read/write access to the picked file(s)
                    foreach (StorageFile file2 in files)
                    {
                        if (String.Compare(file2.FileType, ".hex", true) == 0)
                            hexfiles.Add(file2);
                    }
                }
            }

            var fal = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList;
            MainPage.sSeggerDirToken = fal.Add(sf, "firmwarelist");
            rootPage.storagefileSegger = await fal.GetFolderAsync(MainPage.sSeggerDirToken);
            SeggerDirectoryPath.Text = rootPage.storagefileSegger.Path.ToString();
            ApplicationData.Current.LocalSettings.Values.Remove("segger_dir");
            ApplicationData.Current.LocalSettings.Values.Add("segger_dir", MainPage.sSeggerDirToken);
        }

        private async void ZebraDirectory_Click(object sender, RoutedEventArgs e)
        {
            FolderPicker openPicker = new FolderPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".hex");

            StorageFolder sf = await openPicker.PickSingleFolderAsync();

            var fal = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList;
            MainPage.sZebraDirToken = fal.Add(sf, "zebradirectory");
            rootPage.storagefileZebra = await fal.GetFolderAsync(MainPage.sZebraDirToken);
            ZebraDirectoryPath.Text = rootPage.storagefileZebra.Path.ToString();
            ApplicationData.Current.LocalSettings.Values.Remove("zebra_dir");
            ApplicationData.Current.LocalSettings.Values.Add("zebra_dir", MainPage.sZebraDirToken);
        }

        private async void DatabaseDirectory_Click(object sender, RoutedEventArgs e)
        {
            /*
            FolderPicker openPicker = new FolderPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".db");

            StorageFolder sf = await openPicker.PickSingleFolderAsync();

            var fal = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList;
            MainPage.sDBDirToken = fal.Add(sf, "DBSourceDirectory");
            rootPage.storagefileDB = await fal.GetFolderAsync(MainPage.sDBDirToken);
            DatabaseDirectoryPath.Text = rootPage.storagefileDB.Path.ToString();
            ApplicationData.Current.LocalSettings.Values.Remove("database_dir");
            ApplicationData.Current.LocalSettings.Values.Add("database_dir", MainPage.sDBDirToken);
            */
        }

        private async void FirmwareDirectory_Click(object sender, RoutedEventArgs e)
        {
            FolderPicker openPicker = new FolderPicker();
            openPicker.ViewMode = PickerViewMode.List;
            openPicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            openPicker.FileTypeFilter.Add(".hex");

            StorageFolder sf = await openPicker.PickSingleFolderAsync();

            var fal = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList;
            MainPage.sFirmwareDirToken = fal.Add(sf, "firmwaredirectory");
            rootPage.storagefileFirmware = await fal.GetFolderAsync(MainPage.sFirmwareDirToken);
            FirmwareDirectoryPath.Text = rootPage.storagefileFirmware.Path.ToString();
            ApplicationData.Current.LocalSettings.Values.Remove("firmware_dir");
            ApplicationData.Current.LocalSettings.Values.Add("firmware_dir", MainPage.sFirmwareDirToken);
        }

        private void RSSILevelIn_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RSSILevelOut_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RSSITimeout_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
