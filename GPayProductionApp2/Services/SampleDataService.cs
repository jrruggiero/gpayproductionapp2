﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

using GPayProductionApp.Models;
using GPayProductionApp.Repository;
using GPayProductionApp.Repository.Sql;
using GPayProductionApp2.Services;

namespace GPayProductionApp2.Services
{
    // This class holds sample data used by some generated pages to show how they can be used.
    // TODO WTS: Delete this file once your app is using real data.
    public static class SampleDataService
    {
        public static async Task<IEnumerable<SalesOrders>> GetAllSalesOrders()
        {
            var orders = await App.Repository.SalesOrders.GetAsync();
            return orders;
        }

        public static async Task<IEnumerable<SalesOrders>> GetSalesOrder(string sSearchCriteria)
        {
            var ordersRet = await App.Repository.SalesOrders.GetAsync(sSearchCriteria);
            return ordersRet;
        }

        public static async Task<SalesOrders> GetSalesOrder(long id)
        {
            var orderRet = await App.Repository.SalesOrders.GetAsync(id);
            return orderRet;
        }

        public static async Task<SalesOrders> UpdateSalesOrder(SalesOrders order)
        {
            var orderRet = await App.Repository.SalesOrders.UpsertAsync(order);
            return orderRet;
        }

        public static async Task<SalesOrders> DeleteSalesOrder(long id)
        {
            var orderRet = await App.Repository.SalesOrders.DeleteAsync(id);
            return orderRet;
        }

        public static async Task<SalesOrders> DeleteSalesOrder(SalesOrders order)
        {
            var orderRet = await App.Repository.SalesOrders.DeleteAsync(order);
            return orderRet;
        }

        public static async Task<IEnumerable<FirmwareSource>> GetAllFirmwareSource()
        {
            var orders = await App.Repository.FirmwareSource.GetAsync();
            return orders;
        }

        public static async Task<IEnumerable<FirmwareSource>> GetAllFirmwareSource(string sPartNumberApplicable,
                                                                                   string sPartNumberRevisionApplicable,
                                                                                   int iActive)
        {
            var orders = await App.Repository.FirmwareSource.GetAsync(sPartNumberApplicable,
                                                                      sPartNumberRevisionApplicable,
                                                                      iActive);
            return orders;
        }

        public static async Task<IEnumerable<LabelInterimResult>> GetLabelInterimResult()
        {
            var orders = await App.Repository.LabelInterimResult.GetAsync();
            return orders;
        }

        public static async Task<LabelInterimResult> DeleteLabelInterimResult(long id)
        {
            var item = await App.Repository.LabelInterimResult.DeleteAsync(id);
            return item;
        }

        /*
        public static async Task<LabelInterimResult> DeleteLabelInterimResult(LabelInterimResult result)
        {
            var item = await App.Repository.LabelInterimResult.DeleteAsync(result);
            return item;
        }
        */
        public static async Task<IEnumerable<ProgrammerResults>> GetProgrammerResults()
        {
            var results = await App.Repository.ProgrammerResults.GetAsync();
            return results;
        }

        public static async Task<LabelInterimResult> GetLabelInterimResult(long id)
        {
            var Ret = await App.Repository.LabelInterimResult.GetAsync(id);
            return Ret;
        }

        public static async Task<LabelInterimResult> UpdateLabelInterimResult(LabelInterimResult label)
        {
            var Ret = await App.Repository.LabelInterimResult.UpsertAsync(label);
            return Ret;
        }

        public static async Task<ProgrammerResults> UpdateProgrammerResults(ProgrammerResults result)
        {
            var Ret = await App.Repository.ProgrammerResults.UpsertAsync(result);
            return Ret;
        }

        public static async Task<TestResults> UpdateTestResults(TestResults result)
        {
            var Ret = await App.Repository.TestResults.UpsertAsync(result);
            return Ret;
        }

        public static async Task<FinalResults> UpdateFinalResults(FinalResults result)
        {
            var Ret = await App.Repository.FinalResults.UpsertAsync(result);
            return Ret;
        }
    }
}
