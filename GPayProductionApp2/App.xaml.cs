﻿using System;

using GPayProductionApp2.Services;

using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;

using GPayProductionApp.Models;
using GPayProductionApp.Repository;
using GPayProductionApp.Repository.Sql;

using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.IO;
using Windows.ApplicationModel;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.AccessCache;
using System.Threading.Tasks;


using System.Text;

using System.Collections.Generic;

namespace GPayProductionApp2
{
    public sealed partial class App : Application
    {
        private Lazy<ActivationService> _activationService;

        static StorageFile filef;

        /// <summary>
        /// Pipeline for interacting with backend service or database.
        /// </summary>
        public static IGPayProductionAppRepository Repository { get; private set; }

        private ActivationService ActivationService
        {
            get { return _activationService.Value; }
        }

        public App()
        {
            InitializeComponent();

            // Deferred execution until used. Check https://msdn.microsoft.com/library/dd642331(v=vs.110).aspx for further info on Lazy<T> class.
            _activationService = new Lazy<ActivationService>(CreateActivationService);
        }

        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            if (!args.PrelaunchActivated)
            {
                await ActivationService.ActivateAsync(args);
            }

            // Load the database.

            if (ApplicationData.Current.LocalSettings.Values.TryGetValue(
                "data_source", out object dataSource))
            {
                switch (dataSource.ToString())
                {
                    case "Rest": UseRest(); break;
                    default: UseSqlite(); break;
                }
            }
            else
            {
                UseSqlite();
            }
        }

        protected override async void OnActivated(IActivatedEventArgs args)
        {
            await ActivationService.ActivateAsync(args);
        }

        private ActivationService CreateActivationService()
        {
            return new ActivationService(this, typeof(Views.MainPage), new Lazy<UIElement>(CreateShell));
        }

        private UIElement CreateShell()
        {
            return new Views.ShellPage();
        }

        /// <summary>
        /// Configures the app to use the Sqlite data source. If no existing Sqlite database exists, 
        /// loads a demo database filled with fake data so the app has content.
        /// </summary>
        public async static void UseSqlite()
        {
            string demoDatabasePath = Package.Current.InstalledLocation.Path + @"\Assets\GPayProductionApp.db";
            //string databasePath = ApplicationData.Current.LocalFolder.Path + @"\GPayProductionApp.db";
            string databasePathActive = ApplicationData.Current.GetPublisherCacheFolder("GPayProductionDatabase").Path + @"\GPayProductionApp.db";

            /*
            string sDBDirToken = "";
            StorageFolder storagefileDB = null;
            StorageFile storagefile2DB = null;

            var fal = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList;

            if (ApplicationData.Current.LocalSettings.Values.TryGetValue("database_dir", out object DBDir))
            {
                sDBDirToken = DBDir.ToString();
                storagefileDB = await fal.GetFolderAsync(sDBDirToken);
            }

            storagefile2DB = await storagefileDB.GetFileAsync("GPayProductionApp.db");
            */

            if (!File.Exists(databasePathActive))
                File.Copy(demoDatabasePath, databasePathActive);
            // else
            //    File.Delete(databasePath);

            var dbOptions = new DbContextOptionsBuilder<GPayProductionAppContext>().UseSqlite("Data Source=" +  databasePathActive);
            
            Repository = new SqlGPayProductionAppRepository(dbOptions);
        }

        /// <summary>
        /// Configures the app to use the REST data source. For convenience, a read-only source is provided. 
        /// You can also deploy your own copy of the REST service locally or to Azure. See the README for details.
        /// </summary>
        public static void UseRest()
        {
 //           Repository = new RestContosoRepository("https://customers-orders-api-prod.azurewebsites.net/api/");
        }
    }
}
